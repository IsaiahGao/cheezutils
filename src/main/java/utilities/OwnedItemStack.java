package utilities;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import utilities.particles.ParticleEffects;

public class OwnedItemStack {
    
    public OwnedItemStack(ItemStack item, UUID id, int delayTicks, boolean sparkle) {
        this.item = item;
        this.owner = id;
        this.delay = delayTicks;
        this.sparkle = sparkle;
    }
    
    private Item itementity;
    private ItemStack item;
    private UUID owner;
    private int delay;
    private boolean sparkle;
    
    private Listener list;
    private BukkitTask task;
    
    public OwnedItemStack drop(Location loc) {
    	return this.drop(loc, new Vector());
    }
    
    public OwnedItemStack drop(Location loc, Vector vel) {
        itementity = loc.getWorld().dropItem(loc, item);
        
        if (sparkle) {
            new BukkitRunnable() {
                int cycle = 0;
                @Override
                public void run() {
                    if (++cycle > delay) {
                        cancel();
                    }
                    
                    if (!itementity.isValid()) {
                        cancel();
                        return;
                    }

                    double x = (Math.random() - 0.5) * 0.4;
                    double z = (Math.random() - 0.5) * 0.4;
                    ParticleEffects.END_ROD.display(new Vector(0, 1, 0), (float) ((Math.random() * 0.5 + 0.5) * ((x * x + z * z) / 0.04)), itementity.getLocation().add(x, 0, z), 256);
                }
            }.runTaskTimer(Utils.pl, 1L, 1L);
        }
        
        task = new BukkitRunnable() {
            @Override
            public void run() {
                HandlerList.unregisterAll(list);
            }
        }.runTaskLater(Utils.pl, delay);
        
        list = new Listener() {
            @EventHandler
            public void pickup(EntityPickupItemEvent e) {
                if (e.getItem().equals(itementity)) {
                    if (e.getEntity().getUniqueId().equals(owner)) {
                        HandlerList.unregisterAll(this);
                        task.cancel();
                    } else {
                        e.setCancelled(true);
                    }
                }
            }
            
            @EventHandler
            public void hopper(InventoryPickupItemEvent e) {
                if (e.getItem().equals(itementity)) {
                    e.setCancelled(true);
                }
            }
            
            @EventHandler
            public void itemMerge(ItemMergeEvent e) {
            	if (e.getEntity().equals(itementity)) {
            		e.setCancelled(true);
            	}
            }
        };
        
        Bukkit.getPluginManager().registerEvents(list, Utils.pl);
        return this;
    }

}
