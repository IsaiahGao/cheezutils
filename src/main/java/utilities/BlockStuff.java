package utilities;

import org.bukkit.Material;

public class BlockStuff {
	
	public static boolean isLog(Material m) {
		if (m == null)
			return false;
		
		return m.toString().endsWith("_LOG");
	}
	
	public static boolean isAxeable(Material m) {
		return isLog(m) || m == Material.BROWN_MUSHROOM_BLOCK || m == Material.RED_MUSHROOM_BLOCK;
	}
	
	public static boolean isLeaf(Material m) {
		return m.toString().endsWith("_LEAVES") || m.toString().endsWith("_LEAVES_2");
	}

}