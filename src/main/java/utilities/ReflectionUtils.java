package utilities;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;

public class ReflectionUtils {
	
	public static Class<?> getNMSClass(String name) {
		String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + ".";
		String className = "net.minecraft.server." + version + name;
		Class<?> clazz = null;
			try {
				clazz = Class.forName(className);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		return clazz;
	}
	 
	public static Class<?> getOrgBukkitClass(String name) {
		String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + ".";
		String className = "org.bukkit.craftbukkit." + version + name;
		Class<?> clazz = null;
			try {
				clazz = Class.forName(className);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		return clazz;
	}
	
    public static void modifyField(Object subject, String field, Object newValue) {
        try {
            Field f = subject.getClass().getDeclaredField(field);
            f.setAccessible(true);
            f.set(subject, newValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static Object getPrivateField(Object object, String field) {
        return getPrivateField(object.getClass(), object, field);
    }
    
    public static Object getPrivateField(Class<?> clazz, Object object, String field) {
        try {
            Field f = clazz.getDeclaredField(field);
            f.setAccessible(true);
            return f.get(object);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
