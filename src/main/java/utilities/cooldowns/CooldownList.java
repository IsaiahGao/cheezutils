package utilities.cooldowns;

import java.util.HashMap;
import java.util.Map;

public class CooldownList {
    
    public CooldownList() {
        this.cooldowns = new HashMap<>();
    }

    private Map<String, Long> cooldowns;
    
    public long cooldown(String s, long millis) {
        long l = cooldowns.getOrDefault(s, -1L);
        long diff = System.currentTimeMillis() - l;
        if (l == -1 || diff >= millis) {
            cooldowns.put(s, System.currentTimeMillis());
        }
        return l == -1 ? Long.MAX_VALUE : diff;
    }

}
