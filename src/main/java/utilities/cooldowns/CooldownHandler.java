package utilities.cooldowns;

import java.util.Map;
import java.util.WeakHashMap;

import org.bukkit.entity.LivingEntity;

public class CooldownHandler {
    
    private static Map<LivingEntity, CooldownList> cooldowns = new WeakHashMap<>();
    
    /**
     * @return Milliseconds since ability was last used.
     */
    public static long cooldown(LivingEntity p, String s, long millis) {
        CooldownList list = cooldowns.get(p);
        if (list == null)
            cooldowns.put(p, list = new CooldownList());
        return list.cooldown(s, millis);
    }

    /**
     * @return Milliseconds since ability was last used.
     */
    public static long cooldown(LivingEntity p, String s, int ticks) {
        return cooldown(p, s, ticks * 50L);
    }

    /**
     * @return Whether or not cooldown is done.
     */
    public static boolean cooledDown(LivingEntity p, String s, long millis) {
        return cooldown(p, s, millis) >= millis;
    }

}
