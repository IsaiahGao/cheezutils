package utilities;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import net.minecraft.server.v1_13_R2.NBTBase;
import net.minecraft.server.v1_13_R2.NBTTagCompound;
import net.minecraft.server.v1_13_R2.NBTTagFloat;
import net.minecraft.server.v1_13_R2.NBTTagInt;
import net.minecraft.server.v1_13_R2.NBTTagList;
import net.minecraft.server.v1_13_R2.NBTTagString;

public class AttributeUtils {
    
    public static AttributeBuilder builder() {
        return new AttributeBuilder();
    }
    
    public enum AttributeType {
        MAX_HEALTH("generic.maxHealth", 3, 67),
        MOVEMENT_SPEED("generic.movementSpeed", 5, 84),
        ATTACK_DAMAGE("generic.attackDamage", 894654, 2872),
        ATTACK_SPEED("generic.attackSpeed", 13, 59),
        KNOCKBACK_RESISTANCE("generic.knockbackResistance", 4, 78),
        ARMOR("generic.armor", 32, 46),
        ARMOR_TOUGHNESS("generic.armorToughness", 6, 88),
        LUCK("generic.luck", 1563, 3121);
        
        private AttributeType(String key, int least, int most) {
            this.key = key;
            this.least = least;
            this.most = most;
        }
        
        public String key;
        public int least, most;
        
        private static Map<String, AttributeType> reverse = new HashMap<>();
        static {
        	for (AttributeType type : AttributeType.values())
        		reverse.put(type.key, type);
        }
        
        public static AttributeType getForName(String s) {
        	return reverse.get(s);
        }
    }
    
    public enum Operation {
        ADD(0),
        MULTIPLY(1),
        WIERD_MULTIPLY(2);
        
        private Operation(int id) {
            this.id = id;
        }
        
        public int id;
    }
    
    public enum Slot {
        MAINHAND("mainhand"),
        OFFHAND("offhand"),
        HEAD("head"),
        TORSO("chest"),
        LEGS("legs"),
        FEET("feet"),
        VARIABLE("");
        
        private Slot(String id) {
            this.id = id;
        }
        
        public String id;
        
        public static Slot get(ItemStack item) {
        	String s = item.getType().toString();
        	if (s.endsWith("_HELMET")) return Slot.HEAD;
        	if (s.endsWith("_CHESTPLATE")) return Slot.TORSO;
        	if (s.endsWith("_LEGGINGS")) return Slot.LEGS;
        	if (s.endsWith("_BOOTS")) return Slot.FEET;
        	return Slot.MAINHAND;
        }
        
    }
    
    public static class AttributeBuilder {
        private AttributeBuilder() {
            this.modifiers = new LinkedList<>();
        }
        
        private List<AttributeModifier> modifiers;
        
        public AttributeBuilder with(AttributeType type, Operation op, float amt, Slot slot, int offsetid) {
            if (amt != 0) {
            	for (Iterator<AttributeModifier> it = modifiers.iterator(); it.hasNext();) {
            		AttributeModifier next = it.next();
            		if (next.type == type && next.op == op) {
            			if (amt > next.amt) {
            				// added a more powerful version of the same modifier, so let's update it
            				it.remove();
            				break;
            			}
            			// otherwise no change
            			return this;
            		}
            	}
                modifiers.add(new AttributeModifier(type, op, amt, slot, offsetid));
            }
            return this;
        }
        
        public AttributeBuilder with(AttributeType type, Operation op, float amt, Slot slot) {
            return this.with(type, op, amt, slot, 0);
        }
        
        public ItemStack apply(ItemStack item) {
        	return this.apply(item, true);
        }
        
        public ItemStack apply(ItemStack item, boolean overwrite) {
            net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
            NBTTagCompound compound = nmsStack.hasTag() ? nmsStack.getTag() : new NBTTagCompound();
            NBTTagList modifiers = compound.hasKey("AttributeModifiers") && !overwrite ? compound.getList("AttributeModifiers", 10) : new NBTTagList();

        	Map<AttributeType, Float> defaults = DefaultAttributes.getDefaultAttributes(item);
        	if (!modifiers.isEmpty() && !overwrite) {
	            for (Iterator<AttributeModifier> thisit = this.modifiers.iterator(); thisit.hasNext();) {
	            	AttributeModifier mod = thisit.next();
	            	
	            	// remove if already there
	            	float amtthere = Float.MIN_VALUE;
            		// iterate through attributes currently in the item
	            	for (Iterator<NBTBase> it = modifiers.iterator(); it.hasNext();) {
	            		NBTBase nbt = it.next();
	            		if (nbt instanceof NBTTagCompound) {
	            			NBTTagCompound tag = (NBTTagCompound) nbt;
	            			String attrname = tag.getString("AttributeName");
	            			// remove from defaults since it exists here
	            			defaults.remove(AttributeType.getForName(attrname));

	            			int operation = tag.getInt("Operation");
	            			amtthere = tag.getFloat("Amount");
	            			// remove the mod from our modifiers if its lower levelled
	            			if (attrname.equalsIgnoreCase(mod.type.toString()) && operation == mod.op.id) {
		            			amtthere = tag.getFloat("Amount");
	            				if (mod.amt < amtthere) {
	            					// the ench there is better, so remove our current mod
	            					thisit.remove();
	            					break;
	            				}
	            				
	            				// the ench there is worse, so remove it from item immediately
	            				// and we can add it again later
	            				it.remove();
	            				break;
	            			}
	            		}
	            	}
	            }
            }

            for (AttributeModifier am : this.modifiers) {
        		modifiers.add(am.compile(item));
            }

	        if (!overwrite) {
	            // add back default multipliers that weren't overwritten
	        	for (Map.Entry<AttributeType, Float> entry : defaults.entrySet()) {
	        		modifiers.add(new AttributeModifier(entry.getKey(), Operation.ADD, entry.getValue(), Slot.VARIABLE, ((entry.getKey().ordinal() + 7) * 1031 + Slot.get(item).ordinal() + 4) * 31).compile(item));
	        	}
        	}
        	
            compound.set("AttributeModifiers", modifiers);
            nmsStack.setTag(compound);
            return CraftItemStack.asCraftMirror(nmsStack);
        }
        
        public ItemStack remove(ItemStack item) {
            net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
            if (!nmsStack.hasTag()) {
            	return item;
            }
            
            NBTTagCompound compound = nmsStack.getTag();
            if (!compound.hasKey("AttributeModifiers")) {
            	return item;
            }
            
            NBTTagList modifiers = compound.getList("AttributeModifiers", 10);
            for (AttributeModifier mod : this.modifiers) {
            	// remove
            	for (Iterator<NBTBase> it = modifiers.iterator(); it.hasNext();) {
            		NBTBase nbt = it.next();
            		if (nbt instanceof NBTTagCompound) {
            			NBTTagCompound tag = (NBTTagCompound) nbt;
            			
            			if (tag.getInt("UUIDLeast") == mod.uuidleast && tag.getInt("UUIDMost") == mod.uuidmost) {
            				it.remove();
            			}
            		}
            	}
            }
            
            compound.set("AttributeModifiers", modifiers);
            nmsStack.setTag(compound);
            return CraftItemStack.asCraftMirror(nmsStack);
        }
    }
    
    private static class AttributeModifier {
        AttributeModifier(AttributeType type, Operation op, float amt, Slot slot, int offsetid) {
            this.type = type;
            this.op = op;
            this.amt = amt;
            this.slot = slot;
            this.offset = offsetid;
            this.uuidleast = type.least - op.id + offset;
            this.uuidmost = type.most + op.id + offset;
        }
        
        AttributeType type;
        Operation op;
        float amt;
        int offset;
        Slot slot;
        int uuidleast;
        int uuidmost;
        
        NBTTagCompound compile(ItemStack item) {
            NBTTagCompound nbt = new NBTTagCompound();
            nbt.set("AttributeName", new NBTTagString(type.key));
            nbt.set("Name", new NBTTagString(type.key));
            nbt.set("Amount", new NBTTagFloat(amt));
            nbt.set("Operation", new NBTTagInt(op.id));
            nbt.set("UUIDLeast", new NBTTagInt(uuidleast));
            nbt.set("UUIDMost", new NBTTagInt(uuidmost));
            nbt.set("Slot", new NBTTagString(slot == Slot.VARIABLE ? Slot.get(item).id : slot.id));
            return nbt;
        }
        
        @Override
        public String toString() {
        	return type.toString() + " Op:" + op.toString() + " Amt:" + amt + " Offset:" + offset + " Slot:" + slot.toString() + " UIDL:" + uuidleast + " UIDM:" + uuidmost;
        }
    }

}
