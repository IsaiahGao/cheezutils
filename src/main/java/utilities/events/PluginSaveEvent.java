package utilities.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PluginSaveEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private static int GLOBAL_ID = 0;
    
    public PluginSaveEvent() {
        super();
        this.id = GLOBAL_ID++;
    }
    
    private int id;
    
    public int getSequence() {
    	return this.id;
    }
 
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }

}
