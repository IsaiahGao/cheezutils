package utilities;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import net.minecraft.server.v1_13_R2.ItemArmor;
import net.minecraft.server.v1_13_R2.ItemSword;
import net.minecraft.server.v1_13_R2.ItemTool;
import utilities.AttributeUtils.AttributeType;
import utilities.armor.ArmorType;

public class DefaultAttributes {
	
	public static Map<AttributeType, Float> getDefaultArmorValues(ArmorSlot slot, ArmorType type) {
		if (slot == null || type == null)
			return new HashMap<>();
		
		ItemStack stack = type.getItem(slot);
		net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(stack);
		if (nmsStack.getItem() instanceof ItemArmor) {
			ItemArmor armor = (ItemArmor) nmsStack.getItem();
			Map<AttributeType, Float> map = new HashMap<>();
			// fields:
			// int c = armor value
			// float d = armor toughness
			try {
				Field f = ItemArmor.class.getDeclaredField("c");
				f.setAccessible(true);
				map.put(AttributeType.ARMOR, (float) f.getInt(armor));
				
				Field f2 = ItemArmor.class.getDeclaredField("d");
				f2.setAccessible(true);
				map.put(AttributeType.ARMOR_TOUGHNESS, f2.getFloat(armor));
			} catch (Exception e) {
				for (Field f : ItemArmor.class.getDeclaredFields()) {
					f.setAccessible(true);
					try {
						System.out.println(f.getName() + ": " + f.get(armor).getClass().getSimpleName());
					} catch (Exception e2) {
						
					}
				}
				e.printStackTrace();
				return new HashMap<>();
			}
			return map;
		}
		return new HashMap<>();
	}

	/**
	 * @return {attributetype, attributetype} {AttackDamage, AttackSpeed}
	 */
	public static Map<AttributeType, Float> getDefaultAttributes(ItemStack stack) {
		if (Utils.isEmpty(stack))
			return new HashMap<>();
		
		net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(stack);
		if (nmsStack.getItem() instanceof ItemArmor) {
			return getDefaultArmorValues(ArmorSlot.of(stack.getType()), ArmorType.of(stack.getType()));
		}

		Map<AttributeType, Float> map = new HashMap<>();
		if (nmsStack.getItem() instanceof ItemSword) {
			ItemSword sword = (ItemSword) nmsStack.getItem();
			// fields:
			// float a = attack damage
			// float b = attack speed
			try {
				Field f = ItemSword.class.getDeclaredField("a");
				f.setAccessible(true);
				map.put(AttributeType.ATTACK_DAMAGE, f.getFloat(sword));
				
				Field f2 = ItemSword.class.getDeclaredField("b");
				f2.setAccessible(true);
				map.put(AttributeType.ATTACK_SPEED, f2.getFloat(sword));
			} catch (Exception e) {
				e.printStackTrace();
				return new HashMap<>();
			}
			return map;
		}
		
		if (nmsStack.getItem() instanceof ItemTool) {
			ItemTool tool = (ItemTool) nmsStack.getItem();
			// float c = attack damage
			// float d = attack speed 
			try {
				Field f = ItemTool.class.getDeclaredField("c");
				f.setAccessible(true);
				map.put(AttributeType.ATTACK_DAMAGE, f.getFloat(tool));
				
				Field f2 = ItemTool.class.getDeclaredField("d");
				f2.setAccessible(true);
				map.put(AttributeType.ATTACK_SPEED, f2.getFloat(tool));
			} catch (Exception e) {
				e.printStackTrace();
				return new HashMap<>();
			}
			return map;
		}

		return new HashMap<>();
	}

}
