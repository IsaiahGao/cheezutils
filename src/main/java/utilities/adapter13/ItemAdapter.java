package utilities.adapter13;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;

public class ItemAdapter {
	
	private static Map<Material, Map<Integer, Material>> map = new HashMap<>();
	
	/**
	 * @param mat Must start with LEGACY
	 */
	public static Material get(Material LEGACY_mat, int data) {
		try {
			return map.get(LEGACY_mat).get(data);
		} catch (Exception e) {
			return LEGACY_mat;
		}
	}
	
	static {
		Map<Integer, Material> wool = new HashMap<>();
		wool.put(0, Material.WHITE_WOOL);
		wool.put(1, Material.ORANGE_WOOL);
		wool.put(2, Material.MAGENTA_WOOL);
		wool.put(3, Material.LIGHT_BLUE_WOOL);
		wool.put(4, Material.YELLOW_WOOL);
		wool.put(5, Material.LIME_WOOL);
		wool.put(6, Material.PINK_WOOL);
		wool.put(7, Material.GRAY_WOOL);
		wool.put(8, Material.LIGHT_GRAY_WOOL);
		wool.put(9, Material.CYAN_WOOL);
		wool.put(10, Material.PURPLE_WOOL);
		wool.put(11, Material.BLUE_WOOL);
		wool.put(12, Material.BROWN_WOOL);
		wool.put(13, Material.GREEN_WOOL);
		wool.put(14, Material.RED_WOOL);
		wool.put(15, Material.BLACK_WOOL);
		map.put(Material.LEGACY_WOOL, wool);

		Map<Integer, Material> DYE = new HashMap<>();
		DYE.put(15, Material.BONE_MEAL);
		DYE.put(14, Material.ORANGE_DYE);
		DYE.put(13, Material.MAGENTA_DYE);
		DYE.put(12, Material.LIGHT_BLUE_DYE);
		DYE.put(11, Material.DANDELION_YELLOW);
		DYE.put(10, Material.LIME_DYE);
		DYE.put(9, Material.PINK_DYE);
		DYE.put(8, Material.GRAY_DYE);
		DYE.put(7, Material.LIGHT_GRAY_DYE);
		DYE.put(6, Material.CYAN_DYE);
		DYE.put(5, Material.PURPLE_DYE);
		DYE.put(4, Material.LAPIS_LAZULI);
		DYE.put(3, Material.COCOA_BEANS);
		DYE.put(2, Material.CACTUS_GREEN);
		DYE.put(1, Material.ROSE_RED);
		DYE.put(0, Material.INK_SAC);
		map.put(Material.LEGACY_INK_SACK, DYE);

		Map<Integer, Material> STAINED_GLASS = new HashMap<>();
		STAINED_GLASS.put(0, Material.WHITE_STAINED_GLASS);
		STAINED_GLASS.put(1, Material.ORANGE_STAINED_GLASS);
		STAINED_GLASS.put(2, Material.MAGENTA_STAINED_GLASS);
		STAINED_GLASS.put(3, Material.LIGHT_BLUE_STAINED_GLASS);
		STAINED_GLASS.put(4, Material.YELLOW_STAINED_GLASS);
		STAINED_GLASS.put(5, Material.LIME_STAINED_GLASS);
		STAINED_GLASS.put(6, Material.PINK_STAINED_GLASS);
		STAINED_GLASS.put(7, Material.GRAY_STAINED_GLASS);
		STAINED_GLASS.put(8, Material.LIGHT_GRAY_STAINED_GLASS);
		STAINED_GLASS.put(9, Material.CYAN_STAINED_GLASS);
		STAINED_GLASS.put(10, Material.PURPLE_STAINED_GLASS);
		STAINED_GLASS.put(11, Material.BLUE_STAINED_GLASS);
		STAINED_GLASS.put(12, Material.BROWN_STAINED_GLASS);
		STAINED_GLASS.put(13, Material.GREEN_STAINED_GLASS);
		STAINED_GLASS.put(14, Material.RED_STAINED_GLASS);
		STAINED_GLASS.put(15, Material.BLACK_STAINED_GLASS);
		map.put(Material.LEGACY_STAINED_GLASS, STAINED_GLASS);
		
		Map<Integer, Material> STAINED_GLASS_PANE = new HashMap<>();
		STAINED_GLASS_PANE.put(0, Material.WHITE_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(1, Material.ORANGE_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(2, Material.MAGENTA_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(3, Material.LIGHT_BLUE_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(4, Material.YELLOW_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(5, Material.LIME_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(6, Material.PINK_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(7, Material.GRAY_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(8, Material.LIGHT_GRAY_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(9, Material.CYAN_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(10, Material.PURPLE_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(11, Material.BLUE_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(12, Material.BROWN_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(13, Material.GREEN_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(14, Material.RED_STAINED_GLASS_PANE);
		STAINED_GLASS_PANE.put(15, Material.BLACK_STAINED_GLASS_PANE);
		map.put(Material.LEGACY_STAINED_GLASS_PANE, STAINED_GLASS_PANE);

		Map<Integer, Material> TERRACOTTA = new HashMap<>();
		TERRACOTTA.put(0, Material.WHITE_TERRACOTTA);
		TERRACOTTA.put(1, Material.ORANGE_TERRACOTTA);
		TERRACOTTA.put(2, Material.MAGENTA_TERRACOTTA);
		TERRACOTTA.put(3, Material.LIGHT_BLUE_TERRACOTTA);
		TERRACOTTA.put(4, Material.YELLOW_TERRACOTTA);
		TERRACOTTA.put(5, Material.LIME_TERRACOTTA);
		TERRACOTTA.put(6, Material.PINK_TERRACOTTA);
		TERRACOTTA.put(7, Material.GRAY_TERRACOTTA);
		TERRACOTTA.put(8, Material.LIGHT_GRAY_TERRACOTTA);
		TERRACOTTA.put(9, Material.CYAN_TERRACOTTA);
		TERRACOTTA.put(10, Material.PURPLE_TERRACOTTA);
		TERRACOTTA.put(11, Material.BLUE_TERRACOTTA);
		TERRACOTTA.put(12, Material.BROWN_TERRACOTTA);
		TERRACOTTA.put(13, Material.GREEN_TERRACOTTA);
		TERRACOTTA.put(14, Material.RED_TERRACOTTA);
		TERRACOTTA.put(15, Material.BLACK_TERRACOTTA);
		map.put(Material.LEGACY_STAINED_CLAY, TERRACOTTA);

		Map<Integer, Material> BANNER = new HashMap<>();
		BANNER.put(0, Material.WHITE_BANNER);
		BANNER.put(1, Material.ORANGE_BANNER);
		BANNER.put(2, Material.MAGENTA_BANNER);
		BANNER.put(3, Material.LIGHT_BLUE_BANNER);
		BANNER.put(4, Material.YELLOW_BANNER);
		BANNER.put(5, Material.LIME_BANNER);
		BANNER.put(6, Material.PINK_BANNER);
		BANNER.put(7, Material.GRAY_BANNER);
		BANNER.put(8, Material.LIGHT_GRAY_BANNER);
		BANNER.put(9, Material.CYAN_BANNER);
		BANNER.put(10, Material.PURPLE_BANNER);
		BANNER.put(11, Material.BLUE_BANNER);
		BANNER.put(12, Material.BROWN_BANNER);
		BANNER.put(13, Material.GREEN_BANNER);
		BANNER.put(14, Material.RED_BANNER);
		BANNER.put(15, Material.BLACK_BANNER);
		map.put(Material.LEGACY_BANNER, BANNER);

		Map<Integer, Material> CARPET = new HashMap<>();
		CARPET.put(0, Material.WHITE_CARPET);
		CARPET.put(1, Material.ORANGE_CARPET);
		CARPET.put(2, Material.MAGENTA_CARPET);
		CARPET.put(3, Material.LIGHT_BLUE_CARPET);
		CARPET.put(4, Material.YELLOW_CARPET);
		CARPET.put(5, Material.LIME_CARPET);
		CARPET.put(6, Material.PINK_CARPET);
		CARPET.put(7, Material.GRAY_CARPET);
		CARPET.put(8, Material.LIGHT_GRAY_CARPET);
		CARPET.put(9, Material.CYAN_CARPET);
		CARPET.put(10, Material.PURPLE_CARPET);
		CARPET.put(11, Material.BLUE_CARPET);
		CARPET.put(12, Material.BROWN_CARPET);
		CARPET.put(13, Material.GREEN_CARPET);
		CARPET.put(14, Material.RED_CARPET);
		CARPET.put(15, Material.BLACK_CARPET);
		map.put(Material.LEGACY_CARPET, CARPET);

		Map<Integer, Material> FLOWER = new HashMap<>();
		FLOWER.put(0, Material.POPPY);
		FLOWER.put(1, Material.BLUE_ORCHID);
		FLOWER.put(2, Material.ALLIUM);
		FLOWER.put(3, Material.AZURE_BLUET);
		FLOWER.put(4, Material.RED_TULIP);
		FLOWER.put(5, Material.ORANGE_TULIP);
		FLOWER.put(6, Material.WHITE_TULIP);
		FLOWER.put(7, Material.PINK_TULIP);
		FLOWER.put(8, Material.OXEYE_DAISY);
		map.put(Material.LEGACY_RED_ROSE, FLOWER);

		Map<Integer, Material> PLANT = new HashMap<>();
		PLANT.put(0, Material.SUNFLOWER);
		PLANT.put(1, Material.LILAC);
		PLANT.put(2, Material.TALL_GRASS);
		PLANT.put(3, Material.LARGE_FERN);
		PLANT.put(4, Material.ROSE_BUSH);
		PLANT.put(5, Material.PEONY);
		map.put(Material.LEGACY_DOUBLE_PLANT, PLANT);
		
		Map<Integer, Material> coal = new HashMap<>();
		coal.put(0, Material.COAL);
		coal.put(1, Material.CHARCOAL);
		map.put(Material.LEGACY_COAL, coal);
	}

}
