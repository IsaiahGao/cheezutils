package utilities.adapter13;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Sets;

public class ItemStuff {
	
	private static Set<Material> musicDiscs = Sets.newHashSet();
	private static BiMap<Material, Material> saplingsToLeaves = HashBiMap.create();
	static {
		for (Material m : Material.values()) {
			if (m.toString().startsWith("MUSIC_DISC_"))
				musicDiscs.add(m);
		}

		saplingsToLeaves.put(Material.OAK_SAPLING, Material.OAK_LEAVES);
		saplingsToLeaves.put(Material.BIRCH_SAPLING, Material.BIRCH_LEAVES);
		saplingsToLeaves.put(Material.SPRUCE_SAPLING, Material.SPRUCE_LEAVES);
		saplingsToLeaves.put(Material.DARK_OAK_SAPLING, Material.DARK_OAK_LEAVES);
		saplingsToLeaves.put(Material.JUNGLE_SAPLING, Material.JUNGLE_LEAVES);
		saplingsToLeaves.put(Material.ACACIA_SAPLING, Material.ACACIA_LEAVES);
	}
	
	public static Material getSaplingForLeaf(Material leaf) {
		return saplingsToLeaves.inverse().getOrDefault(leaf, Material.OAK_SAPLING);
	}
	
	public static Material getLeafForSapling(Material sapling) {
		return saplingsToLeaves.getOrDefault(sapling, Material.OAK_LEAVES);
	}
	
	public static boolean isMusicDisc(Material m) {
		return musicDiscs.contains(m);
	}
	
	public static boolean isMusicDisc(ItemStack i) {
		if (i == null)
			return false;
		
		return isMusicDisc(i.getType());
	}

}
