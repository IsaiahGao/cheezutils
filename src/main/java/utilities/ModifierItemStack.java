package utilities;

import org.bukkit.inventory.ItemStack;

public interface ModifierItemStack {
    
    public ItemStack modify(ItemStack item);

}
