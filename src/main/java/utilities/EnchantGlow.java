package utilities;

import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

public class EnchantGlow extends Enchantment {
 
	public EnchantGlow() {
		super(new NamespacedKey("cheezutils", "glow"));
	}
	 
	@Override
	public boolean canEnchantItem(ItemStack item) {
		return true;
	}
	 
	@Override
	public boolean conflictsWith(Enchantment other) {
		return false;
	}
	 
	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.ALL;
	}
	 
	@Override
	public int getMaxLevel() {
		return 3;
	}
	 
	@Override
	public String getName() {
		return "Glow";
	}
	 
	@Override
	public int getStartLevel() {
		return 1;
	}
	
    @Override
    public boolean isTreasure() {
        return false;
    }

    @Override
    public boolean isCursed() {
        return false;
    }

 
}