package utilities.grinder;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.metadata.FixedMetadataValue;

import utilities.Utils;

public class DensityTracker implements Listener {

	private static final String META = "spawn_density";
	private static final String FIN_META = "final_density";
	private static final double RANGE = 5;
    public static final BlockFace[] CARDINAL = {BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void spawn(CreatureSpawnEvent e) {
		if (e.isCancelled())
			return;
		
		if (e.getSpawnReason() != SpawnReason.CHUNK_GEN && e.getSpawnReason() != SpawnReason.NATURAL)
			return;
		
		if (e.getEntity() instanceof Monster) {
			int j = getEntitiesNear(e.getEntity()).size();
			e.getEntity().setMetadata(META, new FixedMetadataValue(Utils.pl, j));
		}
	}
	
	public static boolean wasMobGrinder(Player p, Entity e) {
		if (e.hasMetadata("spn"))
			return true;
		
		List<Entity> list = getEntitiesNear(e);
		int j = list.size();
		if (e.hasMetadata(FIN_META)) {
			j = Math.max(j, e.getMetadata(FIN_META).get(0).asInt());
		}
		
		for (Entity en : list) {
			en.setMetadata(FIN_META, new FixedMetadataValue(Utils.pl, j));
		}
		boolean flag = j > 7 || seemsGrinderish(p, e);
		
		if (flag) {
			System.out.println("Detected mob grinding (I) at: " + Utils.locToSaveableString(e.getLocation()));
		}
		return flag;
	}
	
	private static boolean seemsGrinderish(Player p, Entity e) {
		if (!((LivingEntity) e).hasLineOfSight(p))
			return true;
		
		if (e.getLocation().getBlockY() > 127)
			return true;
		
		if (e.getLocation().getBlock().getType() == Material.CAVE_AIR && e.getLocation().getBlock().getRelative(BlockFace.UP).getType() == Material.CAVE_AIR) {
			return false;
		}
		
        // possible mob grinder
        Block head = ((LivingEntity) e).getEyeLocation().getBlock();
        boolean[] bools = new boolean[4];
        int trues = 0;
        for (int r = 1; r < 4; r++) {
            for (int i = 0; i < 4; i++) {
                if (bools[i])
                    continue;
                
                Block rel = head.getRelative(CARDINAL[i], r);
                if (isSolid(rel) || rel.getType() == Material.WATER) {
                    bools[i] = true;
                    ++trues;
                }
            }
        }
        
        if (trues == 4 || (trues == 3 && e.getLocation().getBlock().getType() == Material.WATER)) {
			System.out.println("Detected mob grinding (II) at: " + Utils.locToSaveableString(e.getLocation()));
            return true;
        }
        return false;
	}
	
	private static List<Entity> getEntitiesNear(Entity e) {
		List<Entity> list = new LinkedList<>();
		for (Entity en : e.getNearbyEntities(RANGE, RANGE, RANGE)) {
			if (en != e && en instanceof Monster) {
				list.add(en);
			}
		}
		return list;
	}
	
	private static boolean isSolid(Block b) {
		if (!Utils.passable(b))
			return true;
		
		switch (b.getType()) {
	        case LADDER:
	        case REPEATER:
	        case COMPARATOR:
	        case RAIL:
	        case DETECTOR_RAIL:
	        case ACTIVATOR_RAIL:
	        case POWERED_RAIL:
	        case SNOW:
	        case END_ROD:
	        case WATER:
	        case LAVA:
	        case NETHER_PORTAL:
	        case END_PORTAL:
	        case COBWEB:
	        	return true;
        	default:
        		return false;
		}
	}

}
