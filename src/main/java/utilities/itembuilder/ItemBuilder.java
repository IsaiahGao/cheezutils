package utilities.itembuilder;

import java.util.List;
import java.util.Map;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

import com.google.common.collect.Lists;

import utilities.AttributeUtils.AttributeBuilder;
import utilities.Utils;
import utilities.adapter13.ItemAdapter;

public class ItemBuilder {
    
	@Deprecated
    public ItemBuilder(Material mat, int data) {
        this.item = new ItemStack(ItemAdapter.get(Material.valueOf("LEGACY_" + mat.toString()), data));
    }
	
	public ItemBuilder() {
		this.item = new ItemStack(Material.STONE);
	}
    
    public ItemBuilder(Material mat) {
    	this.item = new ItemStack(mat);
    }
    
    private ItemStack item;
    
    public ItemBuilder setAmount(int amount) {
    	this.item.setAmount(amount);
    	return this;
    }
    
    public ItemBuilder setName(String name) {
        item = Utils.setName(item, name);
        return this;
    }
    
    public ItemBuilder setLore(String... str) {
        List<String> lore = Lists.newArrayList(str);
        ItemMeta meta = item.getItemMeta();
        meta.setLore(lore);
        item.setItemMeta(meta);
        return this;
    }
    
    public ItemBuilder glow() {
    	this.addEnchantment(Utils.glow, 1);
    	return this;
    }
    
    public ItemBuilder setUnbreakable() {
    	ItemMeta meta = item.getItemMeta();
    	meta.setUnbreakable(true);
    	item.setItemMeta(meta);
    	return this;
    }
    
    public ItemBuilder setRepairCost(int amount) {
        net.minecraft.server.v1_13_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(this.item);
        nmsStack.setRepairCost(amount);
        this.item = CraftItemStack.asBukkitCopy(nmsStack);
        return this;
    }
    
    public ItemBuilder addPotionEffects(PotionEffect... eff) {
    	if (item.getType() != Material.POTION) {
    		item.setType(Material.POTION);
    	}

    	PotionMeta pmeta = (PotionMeta) item.getItemMeta();
    	for (int i = 0; i < eff.length; i++)
    		pmeta.addCustomEffect(eff[i], true);
    	item.setItemMeta(pmeta);
    	return this;
    }
    
    public ItemBuilder setPotion(PotionType potiontype, boolean extended, boolean upgraded) {
    	if (item.getType() != Material.POTION) {
    		item.setType(Material.POTION);
    	}
    	
    	if (extended && upgraded) {
    		extended = false;
    	}
    	
    	PotionMeta pmeta = (PotionMeta) item.getItemMeta();
    	pmeta.setBasePotionData(new PotionData(potiontype, extended, upgraded));
    	item.setItemMeta(pmeta);
    	return this;
    }
    
    public ItemBuilder setPotion(PotionEffect... potioneffects) {
    	if (item.getType() != Material.POTION) {
    		item.setType(Material.POTION);
    	}

    	PotionMeta pmeta = (PotionMeta) item.getItemMeta();
    	pmeta.setBasePotionData(new PotionData(PotionType.WATER));
    	for (PotionEffect potioneffect : potioneffects)
    		pmeta.addCustomEffect(potioneffect, true);
    	item.setItemMeta(pmeta);
    	return this;
    }
    
    public ItemBuilder setPotionColor(int r, int g, int b) {
    	if (item.getType() != Material.POTION) {
    		item.setType(Material.POTION);
    	}

    	PotionMeta pmeta = (PotionMeta) item.getItemMeta();
    	pmeta.setColor(Color.fromRGB(r, g, b));
    	item.setItemMeta(pmeta);
    	return this;
    }
    
    public ItemBuilder setSkull(String base64) {
        item.setType(Material.PLAYER_HEAD);
        Utils.setCustomSkullItemEncoded(item, base64);
        return this;
    }
    
    public ItemBuilder registerSkull(Map<String, ItemStack> map) {
    	map.put(((SkullMeta) this.item.getItemMeta()).getOwner(), this.item);
    	return this;
    }
    
    public ItemBuilder setLeatherColor(int r, int g, int b) {
    	if (item.getItemMeta() instanceof LeatherArmorMeta) {
	        LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
	        meta.setColor(Color.fromRGB(r, g, b));
	        item.setItemMeta(meta);
    	}
        return this;
    }
    
    public ItemBuilder addItemFlags(ItemFlag... flags) {
        ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(flags);
        item.setItemMeta(meta);
        return this;
    }
    
    public ItemBuilder setDurability(int dura) {
    	if (item.getItemMeta() instanceof Damageable) {
	    	Damageable meta = (Damageable) item.getItemMeta();
	    	meta.setDamage(dura);
    	}
    	return this;
    }
    
    public ItemBuilder addEnchantment(Enchantment ench, int lvl) {
        item.addUnsafeEnchantment(ench, lvl);
        return this;
    }
    
    public ItemBuilder log() {
    	if (this.item.getItemMeta().hasLore())
    		Utils.customItemLores.add(this.item.getItemMeta().getLore().get(0));
		return this;
    }

    public ItemBuilder setAttributes(AttributeBuilder attr) {
    	return this.setAttributes(attr, true);
    }
    
    public ItemBuilder setAttributes(AttributeBuilder attr, boolean overwrite) {
    	this.item = attr.apply(this.item, overwrite);
    	return this;
    }
    
    public ItemStack build() {
        return item;
    }

}
