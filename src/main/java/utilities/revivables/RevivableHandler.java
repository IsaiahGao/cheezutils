package utilities.revivables;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R2.util.CraftChatMessage;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_13_R2.EntityArmorStand;

public class RevivableHandler {
    
    private static Map<UUID, Location> dead = new HashMap<>();
    private static Map<UUID, Entity> armorStands = new HashMap<>();
    private static Map<UUID, Long> lastRevived = new HashMap<>();
    private static final int MAX_RANGE = 6 * 6;
    private static Method getName;
    
    public static Map<UUID, Location> getDeadPlayers() {
    	return new HashMap<>(dead);
    }
    
    public static void setDead(Player p, Location loc) {
        dead.put(p.getUniqueId(), loc);
    }
    
    public static Location respawn(Player p) {
        return dead.remove(p.getUniqueId());
    }
    
    public static long getLastRevived(Player p) {
    	return lastRevived.get(p.getUniqueId());
    }
    
    public static void setLastRevived(Player p, long time) {
    	lastRevived.put(p.getUniqueId(), time);
    }
    
    public static void removeAll() {
    	for (Entity entity : armorStands.values()) {
    		try {
    			remove(entity);
    		} catch (Exception e) {
    			
    		}
    	}
    }
    
    public static void removeArmorStand(Player p) {
    	try {
    		Entity entity = armorStands.remove(p.getUniqueId());
    		remove(entity);
    	} catch (Exception e) {
    	}
    }
    
    public static void remove(Entity entity) {
		Chunk chk = entity.getLocation().getChunk();
		if (!chk.isLoaded()) {
			chk.load();
		}
		
		for (Entity e : chk.getEntities()) {
			if (e.getCustomName() != null && e.getCustomName().equals(entity.getCustomName())) {
				e.remove();
				break;
			}
		}
    }
    
    public static Player getNearest(Player reviver) {
        if (dead.isEmpty())
            return null;
        
        Location rloc = reviver.getLocation();
        
        Player target = null;
        double ldist = Double.POSITIVE_INFINITY;
        for (Map.Entry<UUID, Location> entry : dead.entrySet()) {
            Location loc = entry.getValue();
            if (loc.getWorld() != rloc.getWorld())
                continue;
            
            double dist = rloc.distanceSquared(loc);
            OfflinePlayer op = Bukkit.getOfflinePlayer(entry.getKey());
            if (op.isOnline() && dist < MAX_RANGE && dist < ldist) {
                ldist = dist;
                target = op.getPlayer();
            }
        }
        
        return target;
    }
    
    private static Material[] top = {
            Material.COBBLESTONE_WALL, Material.COBBLESTONE_STAIRS, Material.OAK_FENCE, Material.AIR, Material.POTTED_POPPY, Material.POTTED_ALLIUM, Material.POTTED_AZURE_BLUET, Material.POTTED_BLUE_ORCHID
    };
    
    public static void generateGravestone(Player p) {
    	Location loc = p.getLocation();
        final EntityArmorStand nmsStand = new EntityArmorStand(((CraftWorld) loc.getWorld()).getHandle());
        nmsStand.setInvisible(true);
        
        nmsStand.setInvulnerable(true);
        nmsStand.setSmall(true);

        String text = "☠ §7";
        if (getName == null) {
        	try {
        		getName = Class.forName("cwhitelist.Main").getMethod("getName", OfflinePlayer.class);
        	} catch (Exception e) {
        		e.printStackTrace();
        	}
        }
        if (getName != null) {
        	try {
				text += (String) getName.invoke(null, p);
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
        
        nmsStand.setCustomName(CraftChatMessage.fromStringOrNull(text));
        nmsStand.setCustomNameVisible(true);
        nmsStand.collides = false;

        nmsStand.setLocation(loc.getX(), loc.getY(), loc.getZ(), 0, 0);
        ((CraftWorld) loc.getWorld()).getHandle().addEntity(nmsStand);
        
        Entity there = armorStands.get(p.getUniqueId());
        if (there != null)
        	there.remove();
        armorStands.put(p.getUniqueId(), nmsStand.getBukkitEntity());
        
    	
        /*Block b = p.getLocation().getBlock();
        if (Utils.passable(b)) {
            b.setType(Material.MOSSY_STONE_BRICKS);
            
            Block bt = b.getRelative(BlockFace.UP);
            if (Utils.passable(bt)) {
                Material m = Utils.get(top);
                if (m != Material.AIR) {
                    bt.setType(m);
                }
            }
        }
        
        Block bs = null;
        for (BlockFace bf : BreakerAbstract.flat_directions) {
            Block rel = b.getRelative(bf);
            if (Utils.passable(rel)) {
                rel.setType(Material.WALL_SIGN);
                ((Directional) rel.getState()).setFacing(bf);
                
                Sign sign = (Sign) rel.getState();
                sign.setLine(0, "§m-----------------");
                sign.setLine(1, "§lRIP");
                sign.setLine(2, cwhitelist.Main.getName(p));
                sign.setLine(3, "§m-----------------");
                sign.update();
                bs = rel;
                break;
            }
        }
        
        if (bs != null) {
            Block below = bs.getRelative(BlockFace.DOWN);
            switch (below.getType()) {
                case DIRT:
                case GRASS:
                case STONE:
                case COBBLESTONE:
                case GRAVEL:
                case SAND:
                case WATER:
                case AIR:
                case CLAY:
                    below.setType(Material.COARSE_DIRT);
                    break;
                default:
                	if(below.getType().toString().endsWith("_TERRACOTA")) {
                		below.setType(Material.COARSE_DIRT);
                	}
                	
                    break;
            }
        }*/
    }

}
