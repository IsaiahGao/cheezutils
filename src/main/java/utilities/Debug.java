package utilities;

import java.util.Map;

public class Debug {
	
	public static <K, V> void printMap(Map<K, V> map) {
		for (Map.Entry<K, V> entry : map.entrySet()) {
			System.out.println(" {" + entry.getKey().toString() + " -> " + entry.getValue().toString() + "}");
		}
	}
	
	public static <T> void printList(Iterable<T> it) {
		for (T t : it) {
			System.out.println(t.toString());
		}
	}

}
