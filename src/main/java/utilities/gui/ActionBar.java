package utilities.gui;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_13_R2.ChatMessageType;
import net.minecraft.server.v1_13_R2.IChatBaseComponent;
import net.minecraft.server.v1_13_R2.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_13_R2.PacketPlayOutChat;

public class ActionBar implements ActionManager {

	@Override
    public void sendMessage(Player p, String message) {
		IChatBaseComponent icbc = ChatSerializer.a("{\"text\": \""+ChatColor.translateAlternateColorCodes('&', message.replace("\"", "\\\""))+"\"}");
		PacketPlayOutChat bar = new PacketPlayOutChat(icbc, ChatMessageType.a((byte) 2));
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(bar);
	}

}
