package utilities.armor;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import utilities.ArmorSlot;

public enum ArmorType {
	
	LEATHER(Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS),
	GOLD(Material.GOLDEN_HELMET, Material.GOLDEN_CHESTPLATE, Material.GOLDEN_LEGGINGS, Material.GOLDEN_BOOTS),
	CHAIN(Material.CHAINMAIL_HELMET, Material.CHAINMAIL_CHESTPLATE, Material.CHAINMAIL_LEGGINGS, Material.CHAINMAIL_BOOTS),
	IRON(Material.IRON_HELMET, Material.IRON_CHESTPLATE, Material.IRON_LEGGINGS, Material.IRON_BOOTS),
	DIAMOND(Material.DIAMOND_HELMET, Material.DIAMOND_CHESTPLATE, Material.DIAMOND_LEGGINGS, Material.DIAMOND_BOOTS);
	
	private ArmorType(Material... mat) {
		this.mat = mat;
	}
	
	private Material[] mat;
	
	public static ArmorType of(Material m) {
		for (ArmorType at : ArmorType.values()) {
			for (Material mm : at.mat) {
				if (mm == m)
					return at;
			}
		}
		return null;
	}
	
	public Material getMaterial(ArmorSlot slot) {
		return mat[slot.ordinal()];
	}
	
	public ItemStack getItem(ArmorSlot slot) {
		return new ItemStack(getMaterial(slot));
	}

}
