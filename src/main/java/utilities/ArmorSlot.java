package utilities;

import java.lang.reflect.Method;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

public enum ArmorSlot {
    HELM(2, "Helmet"),
    CHESTPLATE(3, "Chestplate"),
    LEGGINGS(4, "Leggings"),
    BOOTS(5, "Boots"),
    MAINHAND(6, "MainHand");
    
    private ArmorSlot(int offset, String field) {
        this.offset = offset;
        this.field = field;
    }
    
    private int offset;
    private String field;
    
    public static ArmorSlot of(Material m) {
    	for (ArmorSlot a : ArmorSlot.values()) {
    		if (m.toString().endsWith("_" + a.field.toUpperCase())) {
    			return a;
    		}
    	}
    	return ArmorSlot.MAINHAND;
    }
    
    public ItemStack get(Inventory i) {
        return i.getItem(i.getSize() - offset);
    }
    
    public void set(Inventory i, ItemStack item) {
        i.setItem(i.getSize() - offset, item);
    }
    
    public void damage(LivingEntity le, int amt) {
    	ItemStack there = this.getFor(le);
    	if (Utils.isEmpty(there)) {
    		return;
    	}
    	
    	ItemMeta meta = there.getItemMeta();
    	if (!(there.getItemMeta() instanceof Damageable)) {
    		return;
    	}
    	
    	int unb = there.getEnchantmentLevel(Enchantment.DURABILITY);
    	if (unb > 0 && Math.random() > 0.6 + 0.4 / (unb + 1)) {
    		return;
    	}
    	
    	Damageable dmgb = (Damageable) meta;
    	int dmg = dmgb.getDamage() + amt;
    	if (dmg >= there.getType().getMaxDurability()) {
    		setFor(le, null, 0);
    		le.getWorld().playSound(le.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0f, 1.0f);
    	} else {
    		dmgb.setDamage(dmg);
    		there.setItemMeta(meta);
    	}
    }
    
    public ItemStack getFor(LivingEntity le) {
    	EntityEquipment eq = le.getEquipment();
    	try {
    		Method m = EntityEquipment.class.getMethod("get" + this.field);
    		return (ItemStack) m.invoke(eq);
    	} catch (Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    }
    
    public void setFor(LivingEntity le, ItemStack item, float dropchance) {
    	EntityEquipment eq = le.getEquipment();
    	try {
    		Method m = EntityEquipment.class.getMethod("set" + this.field, ItemStack.class);
    		m.invoke(eq, item);

    		Method m2 = EntityEquipment.class.getMethod("set" + this.field + "DropChance", float.class);
    		m2.invoke(eq, dropchance);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}