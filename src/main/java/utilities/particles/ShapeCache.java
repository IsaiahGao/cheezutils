package utilities.particles;

public class ShapeCache {
    
    public static final double[] SIN_48 = new double[48];
    public static final double[] COS_48 = new double[48];
    public static final double[] SIN_96 = new double[96];
    public static final double[] COS_96 = new double[96];
    
    static {
        for (int i = 0; i < 48; i++) {
            double r = i / 24.0D * Math.PI;
            SIN_48[i] = Math.sin(r);
            COS_48[i] = Math.cos(r);
        }
        
        for (int i = 0; i < 96; i++) {
            double r = i / 48.0D * Math.PI;
            SIN_96[i] = Math.sin(r);
            COS_96[i] = Math.cos(r);
        }
    }

}
