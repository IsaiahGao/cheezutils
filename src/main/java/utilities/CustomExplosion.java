package utilities;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftLivingEntity;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Tameable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import net.minecraft.server.v1_13_R2.DamageSource;
import utilities.particles.ParticleEffects;

public class CustomExplosion {
    
    public static enum DamageScale {
        LINEAR_DECLINE,
        EXPONENTIAL_DECLINE,
        NONE
    }
    
    public CustomExplosion(Entity causer, Location loc, double radius, double dmg) {
    	this.causer = causer;
    	this.loc = loc;
    	this.radius = radius;
    	this.damage = dmg;
    	this.ready = true;
    }
    
    public CustomExplosion withParticles() {
    	this.particlesAndSound = true;
    	return this;
    }
    
    public CustomExplosion withKnockback(float intensity) {
    	this.knockbackIntensity = intensity;
    	return this;
    }
    
    public CustomExplosion withDamageScale(DamageScale scale) {
    	this.dmgScale = scale;
    	return this;
    }
    
    public CustomExplosion shouldHurtPlayers() {
    	this.shouldHurtPlayers = true;
    	return this;
    }
    
    public CustomExplosion shouldHurtMobs() {
    	this.shouldHurtMobs = true;
    	return this;
    }
    
    public CustomExplosion explosive() {
    	this.explosive = true;
    	return this;
    }
    
    public CustomExplosion customArmor() {
    	this.customArmor = true;
    	return this;
    }
    
    public CustomExplosion setCustomArmorFactors(float reductionPerLevel) {
    	this.customArmorReduction = reductionPerLevel;
    	return this;
    }
    
    /**
     * 
     * @param particlesAndSound Create particles/sound?
     * @param loc Location explosion occurs
     * @param damage Damage at explosion center
     * @param knockbackIntensity Knockback intensity
     * @param radius Radius of affected entities
     * @param dmgScale How distance affects damage
     * @param shouldHurtPlayers Does explosion hit players?
     * @param shouldHurtMobs Does explosion hit monsters?
     * 
     */
    public CustomExplosion(final Entity causer, final boolean particlesAndSound, final Location loc, final double damage,
            final float knockbackIntensity, final double radius, final DamageScale dmgScale, boolean shouldHurtPlayers,
            final boolean shouldHurtMobs, final boolean explosive) {
    	this(causer, particlesAndSound, loc, damage, knockbackIntensity, radius, dmgScale, shouldHurtPlayers, shouldHurtMobs, explosive, false);
    }
    
    public CustomExplosion(final Entity causer, final boolean particlesAndSound, final Location loc, final double damage,
            final float knockbackIntensity, final double radius, final DamageScale dmgScale, boolean shouldHurtPlayers,
            final boolean shouldHurtMobs, final boolean explosive, final boolean customarmor) {
    	this.causer = causer;
    	this.particlesAndSound = particlesAndSound;
    	this.loc = loc;
    	this.damage = damage;
    	this.knockbackIntensity = knockbackIntensity;
    	this.radius = radius;
    	this.dmgScale = dmgScale;
    	this.shouldHurtMobs = shouldHurtMobs;
    	this.shouldHurtPlayers = shouldHurtPlayers;
    	this.explosive = explosive;
    	this.customArmor = customarmor;
    	this.ready = true;
    	this.explode();
    }
    
    private boolean ready = false;
    private Entity causer;
    private boolean particlesAndSound = true;
    private Location loc;
    private double damage;
    private float knockbackIntensity;
    private double radius;
    private DamageScale dmgScale = DamageScale.LINEAR_DECLINE;
    private boolean shouldHurtPlayers = false;
    private boolean shouldHurtMobs = false;
    private boolean explosive = false;
    private boolean customArmor = false;
    private float customArmorReduction = 0.05f;
    
    public void explode() {
    	if (!ready) {
    		for (int i = 0; i < 10; i++)
    			System.out.println("Tried to explode before ready!");
    		try {
    			throw new YoureAnIdiotException();
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    		return; 
    	}
    	
        if (particlesAndSound) {
            ParticleEffects.HUGE_EXPLODE.display(0, 0, 0, 0, 1, loc, 256);
            loc.getWorld().playSound(loc, Sound.ENTITY_GENERIC_EXPLODE, 2.0F, 1.0F);
        }
        
        if (shouldHurtPlayers && causer instanceof Player && causer.getWorld().getName().equalsIgnoreCase("shadowrealm"))
            shouldHurtPlayers = false;
        
        Snowball ref = (Snowball) loc.getWorld().spawnEntity(loc.clone(), EntityType.SNOWBALL);
        if (causer != null && causer instanceof LivingEntity) {
            ref.setShooter((LivingEntity) causer);
        }
        for (Entity e : ref.getNearbyEntities(radius, radius, radius)) {
            if (e.getType() == EntityType.SHULKER_BULLET) {
                e.remove();
                continue;
            }
            
            if (e.isDead())
            	continue;
            
            boolean entityIsPlayer = entityIsPlayer(e);
            if ((!shouldHurtPlayers && entityIsPlayer) || (!shouldHurtMobs && !entityIsPlayer)) {
                continue;
            }
            
            if (e instanceof LivingEntity) {
                final LivingEntity l = (LivingEntity) e;
                final double closenessRatio = Math.max((Math.pow(radius, 2) - l.getLocation().distanceSquared(loc)) / Math.pow(radius, 2), 0);
                final double dd = calculateDamage(closenessRatio, damage, dmgScale);
                
                if (!customArmor) {
            		((CraftLivingEntity) l).getHandle().damageEntity(DamageSource.explosion(null), (float) dd);
                } else {
                	int bp = getProtPieces(l, Enchantment.PROTECTION_EXPLOSIONS);
                	int pp = getProtPieces(l, Enchantment.PROTECTION_ENVIRONMENTAL);
                	double reducePercent = this.customArmorReduction * bp - this.customArmorReduction * 0.5 * pp;
                	if (reducePercent > 0.8)
                		reducePercent = 0.8;
                	
                	double amp = l.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE) ? l.getPotionEffect(PotionEffectType.DAMAGE_RESISTANCE).getAmplifier() : 0;
                	double finaldmg = dd * (1 - reducePercent) * (1 - getArmorMult(l, dd)) * (1 - amp * 0.2);
                	if (finaldmg > 0)
                		Utils.damageUnblockable(l, finaldmg);
                }
                
                if (knockbackIntensity > 0)
                    new BukkitRunnable() {
                        Location ref = loc.clone().add(0, -1, 0);
                        @Override
                        public void run() {
                            if (l != null) {
                                Vector v = calculateKnockback(closenessRatio, knockbackIntensity, l, ref);
                                if (v.lengthSquared() > 0.02) {
                                    l.setVelocity(v);
                                }
                            }
                        }
                    }.runTask(Utils.pl);
                    
                l.setNoDamageTicks(0);
            }
        }
        ref.remove();
    }
    
    private static double getArmorMult(LivingEntity le, double dmg) {
    	double armor = le.getAttribute(Attribute.GENERIC_ARMOR).getValue();
    	double tough = le.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).getValue();
    	return Math.min(20, Math.max(armor / 5, armor - dmg / (2.0 + tough / 4))) / 25.0;
    }
    
    private static int getProtPieces(LivingEntity e, Enchantment ench) {
    	return getProt(e.getEquipment().getHelmet(), ench)
    			+ getProt(e.getEquipment().getChestplate(), ench)
    			+ getProt(e.getEquipment().getLeggings(), ench)
    			+ getProt(e.getEquipment().getBoots(), ench);
    }
    
    private static int getProt(ItemStack item, Enchantment ench) {
    	if (Utils.isEmpty(item))
    		return 0;
    	
    	return item.getEnchantmentLevel(ench);
    }
    
    private static boolean entityIsPlayer(Entity e) {
        return e instanceof Player || (e instanceof Tameable && ((Tameable) e).isTamed());
    }
    
    private static double calculateDamage(double closenessRatio, double baseDamage, DamageScale scale) {
        if (closenessRatio <= 0)
            return 0;
        
        switch (scale) {
        case LINEAR_DECLINE:
            return Math.sqrt(closenessRatio) * baseDamage;
        case EXPONENTIAL_DECLINE:
            return closenessRatio * baseDamage;
        default:
            return baseDamage;
        }
    }
    
    private static Vector calculateKnockback(double closenessRatio, float i, Entity target, Location reference) {
        Vector direction = reference.toVector().subtract(target.getLocation().toVector()).normalize().multiply(-1);
        direction.add(new Vector(0, 0.2, 0));
        return direction.multiply(closenessRatio).multiply(i);
    }

}
