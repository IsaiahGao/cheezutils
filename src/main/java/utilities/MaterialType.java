package utilities;

import org.bukkit.Material;

public enum MaterialType {
	
	DIAMOND,
	IRON,
	CHAINMAIL,
	GOLDEN,
	LEATHER,
	
	WOODEN,
	STONE,
	
	OTHER;
	
	public static MaterialType of(Material m) {
		MaterialType chosen = null;
		for (MaterialType type : MaterialType.values()) {
			if (m.toString().startsWith(type.toString())) {
				chosen = type;
				break;
			}
		}
		
		return chosen == null ? MaterialType.OTHER : chosen;
	}

}
