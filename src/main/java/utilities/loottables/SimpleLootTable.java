package utilities.loottables;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.google.common.collect.Lists;

import utilities.Utils;

public class SimpleLootTable<T> implements LootTable<T> {
    
    public SimpleLootTable() {
        this.contents = new ArrayList<>();
    }
    
    @SafeVarargs
    public SimpleLootTable(T... contents) {
        this.contents = Lists.newArrayList(contents);
    }
    
    private ArrayList<T> contents;
    
    @Override
    /**
     * @param chance Number of times to add the item. For a weighted-chance list, I would strongly recommended using WeightedLootTable.
     */
    public SimpleLootTable<T> add(T item, int i) {
        for (int j = 0; j < i; j++)
            contents.add(item);
        return this;
    }
    
    /**
     * Adds <b>one</b> of the specified item to the list of possibilities.
     * @param item The item to add.
     */
    public SimpleLootTable<T> add(T item) {
        return add(item, 1);
    }
    
    @Override
    public T get() {
        return Utils.get(contents);
    }
    
    @Override
    public void clear() {
        contents.clear();
    }
    
    @Override
    public SimpleLootTable<T> clone() {
        SimpleLootTable<T> t = new SimpleLootTable<>();
        t.contents = new ArrayList<>(this.contents);
        return t;
    }

    @Override
    public boolean isEmpty() {
        return this.contents.isEmpty();
    }
    
    @Override
    public boolean contains(T t) {
    	return this.contents.contains(t);
    }

	@Override
	public Set<T> getValues() {
		return new HashSet<>(contents);
	}
    
}
