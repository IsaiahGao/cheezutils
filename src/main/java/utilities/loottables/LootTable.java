package utilities.loottables;

import java.util.Set;

public interface LootTable<T> {
    
    /**
     * Add an item to the loot table.
     * @param t The item to add.
     * @param w The chance or weight, specified by implementation.
     */
    LootTable<T> add(T t, int w);
    
    /**
     * Get a loot item from the table.
     * @return a weighted random value from the loot table.
     */
    T get();
    
    /**
     * Removes all values.
     */
    void clear();
    
    /**
     * @return A deep copy of the loot table.
     */
    LootTable<T> clone();
    
    boolean isEmpty();
    boolean contains(T t);
    Set<T> getValues();

}
