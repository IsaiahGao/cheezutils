package utilities.loottables;

import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import utilities.YoureAnIdiotException;

/**
 * Handles a loot table that can give items from different
 * loot tables by passing another layer of RNG - first, this
 * loot table must select a loot table based on given weights.
 * Then, it selects a value from that loot table by invoking
 * its get() method.
 * Can be used to separate "common," "uncommon," and "rare"
 * tiers of reward items, for instance.
 * 
 * @author Cheez
 *
 * @param <T> Item type.
 */
public class TieredLootTable<U> implements LootTable<U> {
    
    public TieredLootTable() {
        map = new TreeMap<>();
    }
    
    private Random random = new Random();
    private TreeMap<Integer, LootTable<U>> map;
    private int size;
    
    /**
     * Adds a table possibility to the list of tables.
     * @param table The loot table to add.
     * @param weight The weight for the loot table to be picked. Higher weight = more chance to be picked.
     * @throws YoureAnIdiotException if you try to add this table to itself.
     */
    public TieredLootTable<U> addTable(LootTable<U> table, int weight) throws YoureAnIdiotException {
        if (table == this) {
            throw new YoureAnIdiotException("You can't add a TieredLootTable to itself.");
        }
        map.put(size, table);
        size += weight;
        return this;
    }
    
    /**
     * @return A random "weighted" table from the list of tables.
     */
    public LootTable<U> getTable() {
        if (map.isEmpty())
            return null;
        
        Entry<Integer, LootTable<U>> entry = map.floorEntry(random.nextInt(size));
        return entry == null ? null : entry.getValue();
    }
    
    @Override
    @Deprecated
    /**
     * Not used, as this doesn't make sense for this implementation.
     */
    public TieredLootTable<U> add(U u, int w) {
        return this;
    }
    
    @Override
    public U get() {
        return getTable().get();
    }
    
    @Override
    public void clear() {
        map.clear();
        size = 0;
    }
    
    @Override
    public TieredLootTable<U> clone() {
        TieredLootTable<U> t = new TieredLootTable<>();
        t.map = new TreeMap<>();
        t.size = this.size;
        
        for (Map.Entry<Integer, LootTable<U>> entry : this.map.entrySet()) {
            t.map.put(entry.getKey(), entry.getValue().clone());
        }
        return t;
    }

    @Override
    public boolean isEmpty() {
        return this.map.isEmpty();
    }
    

    @Override
    public boolean contains(U u) {
    	for (LootTable<U> table : this.map.values()) {
    		if (table.contains(u))
    			return true;
    	}
    	return false;
    }

	@Override
	public Set<U> getValues() {
		Set<U> set = new HashSet<>();
		for (LootTable<U> lt : this.map.values()) {
			set.addAll(lt.getValues());
		}
		return set;
	}
}
