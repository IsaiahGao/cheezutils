package utilities.loottables;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

/**
 * Handles a loot table that randomly gives items based on a given
 * weight. An item with higher weight has a higher chance of being
 * received with each get() method call.
 * 
 * @author Cheez
 *
 * @param <T> Item type.
 */
public class WeightedLootTable<T> implements LootTable<T> {
    
    private Random random = new Random();
    
    public WeightedLootTable() {
        this.map = new TreeMap<>();
    }
    
    private TreeMap<Integer, T> map;
    private int size;
    
    @Override
    /**
     * @param weight The randomness "weight" of the item. Higher weight = greater chance of receiving.
     */
    public WeightedLootTable<T> add(T item, int weight) {
        if (weight == 0)
            return this;
        
        map.put(this.size, item);
        this.size += weight;
        return this;
    }
    
    @Override
    public T get() {
        if (map.isEmpty())
            return null;
        
        Entry<Integer, T> entry = map.floorEntry(random.nextInt(size));
        return entry == null ? null : entry.getValue();
    }

    @Override
    public void clear() {
        map.clear();
        size = 0;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Weighted Loot Table [\n");
        for (Entry<Integer, T> entry : map.entrySet()) {
            builder.append("{")
            .append(entry.getValue().toString())
            .append(": ")
            .append(entry.getKey())
            .append("},\n");
        }
        builder.append(']');
        return builder.toString();
    }
    
    @Override
    public WeightedLootTable<T> clone() {
        WeightedLootTable<T> t = new WeightedLootTable<>();
        t.map = new TreeMap<>(this.map);
        t.size = this.size;
        return t;
    }
    
    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }
    
    @Override
    public boolean contains(T t) {
    	return this.map.containsValue(t);
    }

	@Override
	public Set<T> getValues() {
		return new HashSet<>(map.values());
	}
    
}
