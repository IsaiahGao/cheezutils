package utilities.loottables;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import java.util.Set;

import utilities.Utils;

/**
 * Handles a loot table that calculates item drops sequentially.
 * Each possibility has its own independent chance of being picked.
 * Calls to get() traverses the list of possibilities in order
 * and immediately return a value if the RNG roll of one succeeds.
 * Possibilities added first are always processed first.
 * 
 * @author Cheez
 *
 * @param <T> Item type.
 */
public class SequentialLootTable<T> implements LootTable<T> {
    
    private static class PredicateLoot<U> {
        Random random = new Random();
        
        PredicateLoot(U item, int chance) {
            this.item = item;
            this.chance = chance;
        }
        
        private U item;
        private int chance;

        boolean rng() {
            return chance < 1 ? true : random.nextInt(chance) == 0;
        }
    }
    
    public SequentialLootTable() {
        this.list = new LinkedList<>();
    }
    
    private LinkedList<PredicateLoot<T>> list;
    private ArrayList<T> defaultItems;
    
    @Override
    /**
     * @param chance Chance the item will be picked upon traversal, calculated as 1-in-chance.
     */
    public SequentialLootTable<T> add(T item, int chance) {
        list.add(new PredicateLoot<T>(item, chance));
        return this;
    }
    
    /**
     * Adds to a list of default items, from which a possibility is chosen at random if nothing from the main list was picked.
     * @param item Item to add.
     */
    public SequentialLootTable<T> addDefault(T item) {
        if (defaultItems == null)
            defaultItems = new ArrayList<T>();
        defaultItems.add(item);
        return this;
    }
    
    @Override
    public T get() {
        for (Iterator<PredicateLoot<T>> it = list.listIterator(); it.hasNext();) {
            PredicateLoot<T> predicate = it.next();
            if (predicate.rng()) {
                return predicate.item;
            }
        }
        if (!defaultItems.isEmpty()) {
            return Utils.get(defaultItems);
        }
        return null;
    }
    
    @Override
    public void clear() {
        this.list.clear();
        this.defaultItems.clear();
    }
    
    @Override
    public SequentialLootTable<T> clone() {
        SequentialLootTable<T> t = new SequentialLootTable<>();
        t.defaultItems = new ArrayList<>(this.defaultItems);
        t.list = new LinkedList<>(this.list);
        return t;
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty() && this.defaultItems.isEmpty();
    }
    
    @Override
    public boolean contains(T t) {
    	return this.list.contains(t);
    }

	@Override
	public Set<T> getValues() {
		Set<T> set = new HashSet<>();
		for (PredicateLoot<T> p : list) {
			set.add(p.item);
		}
		set.addAll(defaultItems);
		return set;
	}

}
