package utilities;

import java.text.DecimalFormat;

public class DecimalFormats {

    public static final DecimalFormat TWO_DEC = new DecimalFormat("0.00");
    public static final DecimalFormat ONE_DEC = new DecimalFormat("0.0");

}
