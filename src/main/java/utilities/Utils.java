package utilities;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.block.Skull;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftArrow;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftItem;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftProjectile;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.util.Vector;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import net.minecraft.server.v1_13_R2.AxisAlignedBB;
import net.minecraft.server.v1_13_R2.BlockPosition;
import net.minecraft.server.v1_13_R2.DamageSource;
import net.minecraft.server.v1_13_R2.EntityInsentient;
import net.minecraft.server.v1_13_R2.EntityItem;
import net.minecraft.server.v1_13_R2.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_13_R2.PathfinderGoalAvoidTarget;
import net.minecraft.server.v1_13_R2.PathfinderGoalSelector;
import net.minecraft.server.v1_13_R2.TileEntitySkull;
import utilities.AttributeUtils.AttributeBuilder;
import utilities.events.PetTeleportEvent;
import utilities.events.PluginSaveEvent;
import utilities.itembuilder.ItemBuilder;
import utilities.revivables.RevivableHandler;

public class Utils extends JavaPlugin {
	
	public static final ItemStack BLANK = new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setName("").build();

	public static Set<String> customItemLores = new HashSet<String>();
	public static Enchantment glow = new EnchantGlow();
	public static List<String> sbags = new ArrayList<String>();
	public static Utils pl;
	
	public static Method getWorldTileEntity;
	public static Method getWorldHandle;
	
    private static TreeMap<Integer, String> map = new TreeMap<Integer, String>();
    private static HashMap<String, Integer> values = new HashMap<String, Integer>();
    private static final String METHYL = "§C§H§3";

    public static World overworld;
    public static World nether;
    public static World end;
    public static World shadowrealm;
    public static World misc;
    public static World creative;
    public static World arena;
    public static Location purgatory;

    static {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");

        for (int i : map.keySet()) {
            values.put(map.get(i), i);
        }
        
        // registers glow
        CraftingListener.ench();
        
        try {
            getWorldHandle = ReflectionUtils.getOrgBukkitClass("CraftWorld").getMethod("getHandle");
            getWorldTileEntity = ReflectionUtils.getNMSClass("WorldServer").getMethod("getTileEntity", BlockPosition.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
//        try {
//            //net.minecraft.server.v1_13_R2.Enchantment.enchantments.a(121, new MinecraftKey("Glow"), new NMSEnchantmentGlow(Rarity.VERY_RARE, EnumItemSlot.values()));
//        	System.out.println("register glow");
//            Enchantment.registerEnchantment(glow);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

	public enum EnumHand {
		MAIN,
		OFF
	}

	@Override
	public void onEnable() {
		pl = this;
		getServer().getPluginManager().registerEvents(new CraftingListener(), this);

		overworld = getServer().getWorld("world");
		nether = getServer().getWorld("world_nether");
		end = getServer().getWorld("world_the_end");
		shadowrealm = getServer().getWorld("shadowrealm");
		misc = getServer().getWorld("misc");
		creative = getServer().getWorld("creative");
		arena = getServer().getWorld("arena");
        purgatory = new Location(misc, -666, 66, -666);
        
        new BukkitRunnable() {
        	@Override
        	public void run() {
        		Bukkit.getPluginManager().callEvent(new PluginSaveEvent());
        	}
        }.runTaskTimer(this, 10 * 60 * 20, 10 * 60 * 20);
	}
	
	@Override
	public void onDisable() {
		RevivableHandler.removeAll();
	}
	
	public static boolean isEmpty(ItemStack item) {
		return item == null || isAir(item);
	}
	
	public static boolean isAir(Material m) {
		return m == Material.AIR || m == Material.CAVE_AIR;
	}
	
	public static boolean isAir(ItemStack item) {
		return isAir(item.getType());
	}
	
	public static boolean isAir(Block b) {
		return isAir(b.getType());
	}
	
	public static boolean hasGlow(ItemStack i) {
		return i.getEnchantments().containsKey(Utils.glow);
	}
	
    public static boolean damageEventIsCancelled(Entity causer, Entity victim) {
        if (causer instanceof Player && victim instanceof Player && !victim.getWorld().getPVP()) {
            return true;
        }
        
        EntityDamageByEntityEvent event = new EntityDamageByEntityEvent(causer, victim, DamageCause.ENTITY_ATTACK, 0.01);
        Bukkit.getPluginManager().callEvent(event);
        return event.isCancelled();
    }
	
	public static void removeItem(Player p, int slot) {
	    ItemStack i = p.getInventory().getItem(slot);
	    if (i.getAmount() > 1)
	        i.setAmount(i.getAmount() - 1);
	    else
	        i = null;
	    p.getInventory().setItem(slot, i);
	}
	
	@Deprecated
	public static void removeItem(Player p) {
		ItemStack i = p.getInventory().getItemInHand();
		if (i.getAmount() > 1)
		    i.setAmount(i.getAmount() - 1);
		else
		    p.getInventory().setItemInHand(i.getType().equals(Material.MUSHROOM_STEW) || i.getType().equals(Material.RABBIT_STEW) ? new ItemStack(Material.BOWL, 1) : null);
		p.updateInventory();
	}
	
	public static void removeItem(Player p, EquipmentSlot hand) {
	    removeItem(p, hand == EquipmentSlot.HAND ? EnumHand.MAIN : EnumHand.OFF);
	}
	
	public static void removeItem(Player p, EnumHand hand) {
		ItemStack i;
		switch (hand) {
		case MAIN:
			i = p.getInventory().getItemInMainHand();
			if (i.getAmount() > 1)
			    i.setAmount(i.getAmount() - 1);
			else
			    p.getInventory().setItemInMainHand(i.getType().equals(Material.MUSHROOM_STEW) || i.getType().equals(Material.RABBIT_STEW) ? new ItemStack(Material.BOWL, 1) : null);
			break;
		case OFF:
			i = p.getInventory().getItemInOffHand();
			if (i.getAmount() > 1)
			    i.setAmount(i.getAmount() - 1);
			else
			    p.getInventory().setItemInOffHand(i.getType().equals(Material.MUSHROOM_STEW) || i.getType().equals(Material.RABBIT_STEW) ? new ItemStack(Material.BOWL, 1) : null);
			break;
		}
		p.updateInventory();
	}

    public static ItemStack colorLeatherArmor(ItemStack i, Color color) {
    	if (i.getType().toString().contains("LEATHER_")) {
    		LeatherArmorMeta meta = (LeatherArmorMeta) i.getItemMeta();
    		meta.setColor(color);
    		i.setItemMeta(meta);
    	}
    	return i;
    }
    
	public static boolean isCustomItem(ItemStack item) {
		if (item == null || item.getType() == Material.AIR || item.getItemMeta().getLore() == null)
			return false;
		String s = item.getItemMeta().getLore().get(0).replace("§r§f", "§f").replace("§r", "§f");
		return customItemLores.contains(s) || sbags.contains(s);
	}
	
	public static ItemStack setItemAmount(ItemStack item, int amount) {
		ItemStack it2 = item.clone();
		it2.setAmount(amount);
		return it2;
	}
	
	public static boolean compareLoreAndItems(ItemStack i1, ItemStack i2, int line) {
		try {
			if (i1.getType() != i2.getType())
				return false;
			
			return compareLore(i1, i2, line);
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean compareLoreAndItems(ItemStack i1, ItemStack i2) {
		try {
			if (i1.getType() != i2.getType())
				return false;
			
			return compareLore(i1, i2);
		} catch (Exception e) {
			return false;
		}
	}
    
    /**
     * @return Whether the 2 items have the same lore
     */
    public static boolean compareLore(ItemStack i1, ItemStack i2) {
    	try {
    	    List<String> l1 = i1.getItemMeta().getLore(),
    	            l2 = i2.getItemMeta().getLore();
    		return (l1 == null && l2 == null) || l1.equals(l2);
    	} catch (Exception e) {
    		return false;
    	}
    }
    
    /**
     * @return Whether the 2 items have the same lore at a certain line, ignoring &r&f's
     */
    public static boolean compareLore(ItemStack i1, ItemStack i2, int line) {
    	try {
    		if (!i1.getItemMeta().hasLore() && !i2.getItemMeta().hasLore()) {
    			return true;
    		}
    		
    		return i1.getItemMeta().getLore().get(line).replace("§r§f", "§f").replace("§r", "§f").equals(i2.getItemMeta().getLore().get(line).replace("§r§f", "§f").replace("§r", "§f"));
    	} catch (Exception e) {
    		return false;
    	}
    }
    
    /**
     * Hides entity from all online players
     */
    public static void hideProjectile(Entity projectile) {
    	for (Player p : Bukkit.getOnlinePlayers())
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(((CraftEntity) projectile).getHandle().getId()));
    }
    
    public static void giveItemRespectStacks(Player p, ItemStack i) {
        int maxstack = i.getType().getMaxStackSize();
        int stacks = i.getAmount() / maxstack;
        for (int j = 0; j < stacks; j++) {
            Utils.giveItem(p, Utils.setItemAmount(i, maxstack));
        }

        if (i.getAmount() % maxstack > 0)
            Utils.giveItem(p, Utils.setItemAmount(i, i.getAmount() % maxstack));
    }
    
    public static void giveItem(Player p, ItemStack item) {
        giveItem(p, item, p.getLocation().add(0.3, 0.2, 0.3));
    }
    
    public static void giveItem(Player p, ItemStack item, Location dropIfFull) {
    	if (item != null)
	    	for (ItemStack overflow : p.getInventory().addItem(item).values())
	    		if (overflow != null)
	    			dropIfFull.getWorld().dropItem(dropIfFull, overflow);
    }
    
    public static ItemStack tradeLores(ItemStack base, ItemStack lores) {
    	List<String> lore = lores.getItemMeta().getLore();
    	
    	ItemMeta meta = base.getItemMeta();
    	meta.setLore(lore);
    	meta.setDisplayName(lores.getItemMeta().getDisplayName());
    	base.setItemMeta(meta);
    	return base;
    }
    
    public static ItemStack setLore(ItemStack item, String... lore) {
    	List<String> list = new ArrayList<String>();
    	for (String str : lore)
    		list.add(str);
    	return setLore(item, list);
    }
    
    public static ItemStack setLore(ItemStack item, List<String> lore) {
    	ItemStack clone = item.clone();
    	ItemMeta meta = clone.getItemMeta();
    	meta.setLore(lore);
    	clone.setItemMeta(meta);
    	return clone;
    }
    
    public static void setLore(ItemStack item, int line, String changeTo) {
    	ItemMeta meta = item.getItemMeta();
    	List<String> lore = meta.getLore();
    	lore.set(line, changeTo);
    	meta.setLore(lore);
    	item.setItemMeta(meta);
    }
    
    public static ItemStack setName(ItemStack item, String name) {
    	ItemStack clone = item.clone();
    	ItemMeta meta = clone.getItemMeta();
    	meta.setDisplayName(name);
    	clone.setItemMeta(meta);
    	return clone;
    }
    
    public static ItemStack constructPotion(Material mat, String name, String[] lore, PotionType type, boolean extended, boolean upgraded) {
        if (mat != Material.POTION && mat != Material.SPLASH_POTION && mat != Material.LINGERING_POTION)
            throw new YoureAnIdiotException();
        
        ItemStack item = new ItemStack(mat);
        PotionMeta meta = (PotionMeta) item.getItemMeta();
        meta.setBasePotionData(new PotionData(type, extended, upgraded));
        if (name != null)
            meta.setDisplayName(name);
        if (lore != null && lore.length > 0)
            meta.setLore(Lists.newArrayList(lore));
        item.setItemMeta(meta);
        return item;
    }
    
    public static ItemStack constructPotion(Material mat, String name, String[] lore, int r, int g, int b, PotionEffect... effects) {
        if (mat != Material.POTION && mat != Material.SPLASH_POTION && mat != Material.LINGERING_POTION)
            throw new YoureAnIdiotException();
        
        ItemStack item = new ItemStack(mat);
        PotionMeta meta = (PotionMeta) item.getItemMeta();
        for (PotionEffect effect : effects)
            meta.addCustomEffect(effect, true);
        meta.setColor(Color.fromRGB(r, g, b));
        if (name != null)
            meta.setDisplayName(name);
        if (lore != null && lore.length > 0)
            meta.setLore(Lists.newArrayList(lore));
        item.setItemMeta(meta);
        return item;
    }
    
    @Deprecated
    public static ItemStack ConstructItemStack(Material mat, int amount, short durability, String displayName, String[] lore) {
    	return ConstructItemStack(mat, amount, durability, displayName, lore, false);
    }
    
    @Deprecated
    public static ItemStack ConstructEnchantedItemStack(Material mat, int amount, short durability, String displayName, String[] lore) {
    	return ConstructEnchantedItemStack(mat, amount, durability, displayName, lore, false);
    }

    public static ItemStack ConstructItemStack(Material mat, int amount, short durability, String displayName, String[] lore, boolean log, AttributeBuilder builder) {
        ItemStack item = ConstructItemStack(mat, amount, durability, displayName, lore, log);
        return builder.apply(item, true);
    }
    
    public static ItemStack ConstructItemStack(Material mat, int amount, short durability, String displayName, String[] lore, boolean log) {
    	ItemStack i = new ItemStack(mat, amount, durability);
    	ItemMeta meta = i.getItemMeta();
    	
    	if (lore != null && lore.length > 0) {
	    	List<String> loreList = new ArrayList<String>();
	    	for (String s : lore)
	    		loreList.add(s.replace("&", "§"));
	    	meta.setLore(loreList);
    	}
    	
    	meta.setDisplayName(displayName.replace("&", "§"));
    	i.setItemMeta(meta);
    	
    	if (log)
    		customItemLores.add(lore[0]);
    	
    	return i;
    }
    
    public static ItemStack ConstructEnchantedItemStack(Material mat, int amount, short durability, String displayName, String[] lore, boolean log) {
    	ItemStack i = ConstructItemStack(mat, amount, durability, displayName, lore, log);
    	i.addEnchantment(glow, 1);
    	return i;
    }

    public static ItemStack ConstructItemStack(Material mat, int amount, short durability, String displayName, String[] lore, boolean log, Enchantment... enchants) {
    	ItemStack i = ConstructItemStack(mat, amount, durability, displayName, lore, log);
    	for (Enchantment ench : enchants) {
    		i.addUnsafeEnchantment(ench, i.containsEnchantment(ench) ? i.getEnchantmentLevel(ench) + 1 : 1);
    	}
    	return i;
    }
    
    public static ItemStack setUnbreakable(ItemStack item) {
    	ItemStack i = item.clone();
    	ItemMeta meta = item.getItemMeta();
    	meta.setUnbreakable(true);
    	i.setItemMeta(meta);
    	return i;
    }
    
    /**
     * @return A random number between [0, range]
     */
	public static int random(int range) {
		return (int) (Math.random() * (range + 1));
	}
	
	public static boolean isInt(String s) {
	    try {
	        Integer.parseInt(s);
	    } catch (NumberFormatException nfe) {
	        return false;
	    }
	    return true;
	}
	
	public static String capitalizeFirst(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}
	
	public static String capitalizeEach(String s) {
	    if (s == null || s.isEmpty())
	        return s;
	    
	    StringBuilder b = new StringBuilder();
        b.append(Character.toUpperCase(s.charAt(0)));
	    for (int i = 1; i < s.length(); i++) {
	        char c = s.charAt(i);
            b.append(Character.toLowerCase(c));
	        if (c == ' ' || c == '_') {
	            b.append(Character.toUpperCase(s.charAt(++i)));
	        }
	    }
	    return b.toString();
	}
	
	public static int parseInt(String s) {
	    return parseInt(s, 0);
	}
	
	public static int parseInt(String s, int defVal) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return defVal;
        }
	}
	
	public static Objective getObjective(String s) {
		Scoreboard board = Bukkit.getServer().getScoreboardManager().getMainScoreboard();
		return board.getObjective(s) == null ? board.registerNewObjective(s, "dummy", s) : board.getObjective(s);
	}
	
	public static void setCustomSkullItemEncoded(ItemStack item, String base64) {
        SkullMeta im = (SkullMeta) item.getItemMeta();
        GameProfile profile = new GameProfile(a(base64), base64.substring(Math.min(0, base64.length() - 10), base64.length() - 1));
        profile.getProperties().put("textures", new Property("textures", base64));
        Field profileField = null;
        try {
            profileField = im.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(im, profile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        item.setItemMeta(im);
	}
	
	private static UUID a(String base64) {
        String sub = base64.substring(base64.length() - 13, base64.length() - 3);
        Random rand = new Random(sub.hashCode());
        byte[] arr = new byte[16];
        rand.nextBytes(arr);
        return UUID.nameUUIDFromBytes(arr);
	}
	
	public static void setCustomSkullItem(ItemStack item, String nonBase64) {
	    setCustomSkullItemEncoded(item, Base64Coder.encodeString("{textures:{SKIN:{url:\"" + nonBase64 + "\"}}}"));
		/*SkullMeta im = (SkullMeta) item.getItemMeta();
		GameProfile profile = new GameProfile(UUID.randomUUID(), nonBase64.substring(nonBase64.length() - 10, nonBase64.length() - 1));
	    String encodedData = Base64Coder.encodeString("{textures:{SKIN:{url:\"" + nonBase64 + "\"}}}");
	    profile.getProperties().put("textures", new Property("textures", encodedData));
	    Field profileField = null;
	    try {
	        profileField = im.getClass().getDeclaredField("profile");
	        profileField.setAccessible(true);
	        profileField.set(im, profile);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    item.setItemMeta(im);*/
	}
	
	public static Block setCustomSkullBlock(String skin, Block skull) {
	    return setCustomSkullBlockEncoded(Base64Coder.encodeString("{textures:{SKIN:{url:\"" + skin + "\"}}}"), skull);
	}
	
	public static Block setCustomSkullBlockEncoded(String skinBase64, Block skull) {
        if (skull.getState() instanceof Skull) {
            try {
                GameProfile profile = new GameProfile(a(skinBase64), skinBase64.substring(skinBase64.length() - 10, skinBase64.length() - 1));
                profile.getProperties().put("textures", new Property("textures", skinBase64));
                Object world = getWorldHandle.invoke(skull.getWorld());
                TileEntitySkull tileSkull = (TileEntitySkull) getWorldTileEntity.invoke(world, new BlockPosition(skull.getX(), skull.getY(), skull.getZ()));
                //Easter.setGameProfile.invoke(tileSkull, profile);
                tileSkull.setGameProfile(profile);
                tileSkull.update();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return skull;
	}
	
	public static String getBase64Texture(Skull skull) {
		try {
			Object world = getWorldHandle.invoke(skull.getWorld());
			TileEntitySkull tileSkull = (TileEntitySkull) getWorldTileEntity.invoke(world, new BlockPosition(skull.getX(), skull.getY(), skull.getZ()));
			Collection<Property> collection = tileSkull.getGameProfile().getProperties().get("textures");
			for (Iterator<Property> it = collection.iterator(); it.hasNext();) {
				Property p = it.next();
				if (p.getName().equals("textures")) {
					String value = p.getValue();
					return value/*.replace("{textures:{SKIN:{url:\"", "").replace("\"}}}", "")*/;
				}
			}
			
		} catch (Exception e) {
			
		}
		return null;
	}
    
    public static <T> T get(List<T> container) {
        if (container.isEmpty())
            return null;
        
        return container.get(random(container.size() - 1));
    }
    
    public static <T> T getAndRemove(List<T> container) {
        if (container.isEmpty())
            return null;
        
        return container.remove(random(container.size() - 1));
    }
	
	public static <T> T get(T[] array) {
	    return array[random(array.length - 1)];
	}
	
	public static <T> T get(Set<T> nonindexed) {
	    int size = nonindexed.size();
	    
	    int rand = random(size - 1);
	    int j = 0;
	    for (T t : nonindexed) {
	        if (j == rand) {
	            return t;
	        }
	        j++;
	    }
	    return null;
	}
	
	public static String makeInvisible(String s) {
	    return s.replaceAll("(.)", "§$1");
	}
	
	public static String makeUninvisible(String s) {
	    s = s.replaceAll("(§§)([^§])", "§$2");
	    return s.replaceAll("(§)([^§])", "$2");
	}
	
	public static String getPrettyItemName(ItemStack stack) {
	    if (stack.getItemMeta().hasDisplayName())
	        return stack.getItemMeta().getDisplayName();
	    if (stack.getType() == Material.GOLDEN_APPLE && stack.getDurability() == 1)
	        return "God_Apple";
	    return capitalizeEach(stack.getType().toString());
	}
	
	public static String locToString(Location loc) {
	    try {
	        return loc.getWorld().getName() + "," + loc.getX() + "," + loc.getY() + "," + loc.getZ();
	    } catch (Exception e ) {
	        e.printStackTrace();
            return "ERROR," + loc.getX() + "," + loc.getY() + "," + loc.getZ();
	    }
	}
	
	public static Location stringToLoc(String loc) {
	    if (loc.equals("null"))
	        return null;
	    
        String[] split = loc.split(",");
        try {
            return new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]));
        } catch (Exception e) {
            System.out.println("INVALID STRING TO LOC: " + loc);
            e.printStackTrace();
            return null;
        }
    }
	
	public static String locToSaveableString(Location loc) {
	    if (loc == null)
	        return "null";
        return (loc.getWorld() == null ? "ERROR" : loc.getWorld().getName()) + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ();
	}

    public static String toRoman(int number) {
        int l =  map.floorKey(number);
        if ( number == l ) {
            return map.get(number);
        }
        return map.get(l) + toRoman(number-l);
    }
    
    public static int fromRoman(String roman) {
        if (roman.isEmpty()) {
            return 0;
        }
        
        int sum = 0;
        char[] arr = roman.toCharArray();
        boolean flag = true;
        for (int i = 0; i < arr.length - 1; i++) {
            int val1 = values.get(arr[i] + "");
            int val2 = values.get(arr[i + 1] + "");
            
            if (val2 > val1) {
                sum += val2 - val1;
                flag = false;
                ++i;
            } else {
                sum += val1;
                flag = true;
            }
        }
        
        if (flag) {
            sum += values.get(arr[arr.length - 1] + "");
        }
        return sum;
    }
    
    public static void teleport(Entity p, Location loc) {
        teleport(p, loc, true);
    }

    public static void teleport(Entity p, Location loc, boolean tppets) {
        // sit all pets
        List<Entity> pets = new LinkedList<>();
        if (tppets) {
            boolean shouldntTeleport = loc.getWorld().getName().equals("arena") || loc.getWorld().getName().equals("creative");
            for (Entity pet : p.getNearbyEntities(30, 30, 30)) {
                if (pet instanceof Tameable && pet instanceof Sittable && ((Tameable) pet).getOwner() == p && !((Sittable) pet).isSitting()) {
                    if (shouldntTeleport)
                        ((Sittable) pet).setSitting(true);
                    else
                        pets.add(pet);
                }
            }
        }
        
        if (!p.getPassengers().isEmpty())
            p.eject();
        
        if (!loc.getChunk().isLoaded())
            loc.getChunk().load();
        
        if (p.getVehicle() != null)
            switch (p.getVehicle().getType()) {
            case HORSE:
            case DONKEY:
            case MULE:
            case SKELETON_HORSE:
            case ZOMBIE_HORSE:
            case LLAMA:
            case MINECART:
            case BOAT:
            case WOLF:
            case OCELOT:
                Entity vehicle = p.getVehicle();
                vehicle.eject();
                vehicle.teleport(loc);
                pets.remove(vehicle); // remove from list of pets to tp since we're tping it here
                
                p.eject();
                p.teleport(vehicle, TeleportCause.PLUGIN);
                //vehicle.addPassenger(p);

                for (Entity pet : pets) {
                    pet.teleport(p);
                }
                return;
                default:
                    break;
            }
        p.teleport(loc);
        for (Entity pet : pets) {
            PetTeleportEvent event = new PetTeleportEvent(pet, pet.getLocation(), loc);
            Bukkit.getPluginManager().callEvent(event);
            
            if (!event.isCancelled())
                pet.teleport(p);
        }
    }
    
    public static String getName(Entity e, String prefix) {
        if (!prefix.isEmpty() && !ChatColor.stripColor(prefix).endsWith(" "))
            prefix = prefix + " ";
        
        if (e.getCustomName() != null)
            return prefix + e.getCustomName();
        
        if (e.getType() == EntityType.OCELOT)
            return prefix + "ocelot";
        
        return prefix + e.getType().toString().toLowerCase();
    }
    
    public static TreeMap<Integer, ItemStack> formatMaterialDataAmountWeight(List<String> strings, String splitter) {
        TreeMap<Integer, ItemStack> map = new TreeMap<>();
        int i = 0;
        for (String line : strings) {
            try {
                String[] arr = line.split(splitter);
                Material mat = Material.getMaterial(arr[0]);
                short data = Short.parseShort(arr[1]);
                int amount = Integer.parseInt(arr[2]);
                int weight = Integer.parseInt(arr[3]);
                ItemStack item = new ItemStack(mat, amount, data);
                map.put(i, item);
                i += weight;
            } catch (Exception e) {
                System.out.println("Error parsing formatted value: '" + line + "'");
            }
        }
        map.put(i, null);
        return map;
    }
    
    public static String barFor(float f, char color, String text) {
        StringBuilder builder = new StringBuilder("§f[§" + color);
        int c = (int) Math.ceil(f * 50);
        for (int i = 0; i < 50; i++) {
            if (c-- <= 0) {
                builder.append("§8");
            }
            
            builder.append(';');
        }
        builder.append("§f]");
        if (text != null)
            builder.append(' ').append(text);
        return builder.toString();
    }

    public static List<String> locListToStringList(Iterable<Location> it) {
        return locListToStringList(it, true);
    }
    
    public static List<String> locListToStringList(Iterable<Location> it, boolean saveable) {
        List<String> list = new ArrayList<>();
        for (Location l : it)
            list.add(saveable ? locToSaveableString(l) : locToString(l));
        return list;
    }
    
    public static Vector toTarget(LivingEntity target, LivingEntity boss) {
        if (target == null)
            return boss.getLocation().getDirection();
        
        Vector dir = target.getLocation().subtract(boss.getLocation()).toVector();
        return dir;
    }
    
    public static Player getNearestPlayer(int range, Entity entity) {
        double dist = Double.POSITIVE_INFINITY;
        Player result = null;
        
        for (Entity e : entity.getNearbyEntities(range, range, range)) {
            if (!(e instanceof Player))
                continue;
            
            Player p = (Player) e;
            double d = p.getLocation().distanceSquared(entity.getLocation());
            if (d < dist) {
                result = p;
                dist = d;
            }
        }
        
        return result;
    }
    
    public static Vector toNearestPlayer(int range, LivingEntity boss) {
        LivingEntity target = boss instanceof Creature ? ((Creature) boss).getTarget() : null;
        if (target == null)
            target = getNearestPlayer(range, boss);
        if (target == null)
            return boss.getLocation().getDirection();
        
        Vector dir = target.getLocation().subtract(boss.getLocation()).toVector();
        return dir;
    }
    
    public static boolean passable(Block b) {
        switch (b.getType()) {
            case AIR:
            case CAVE_AIR:
            case TALL_GRASS:
            case FERN:
            case GRASS:
            case POPPY:
            case DANDELION:
            case SUNFLOWER:
            case ROSE_BUSH:
            case LILAC:
            case PEONY:
            case VINE:
            case LADDER:
            case TORCH:
            case REDSTONE_TORCH:
            case REPEATER:
            case COMPARATOR:
            case REDSTONE_WIRE:
            case HEAVY_WEIGHTED_PRESSURE_PLATE:
            case LIGHT_WEIGHTED_PRESSURE_PLATE:
            case OAK_PRESSURE_PLATE:
            case STONE_PRESSURE_PLATE:
            case RED_MUSHROOM:
            case BROWN_MUSHROOM:
            case RAIL:
            case DETECTOR_RAIL:
            case ACTIVATOR_RAIL:
            case POWERED_RAIL:
            case SNOW:
            case END_ROD:
            case WATER:
            case LAVA:
            case NETHER_PORTAL:
            case END_PORTAL:
            case COBWEB:
                return true;
            default:
                String s = b.getType().toString();
                return s.endsWith("CARPET") || s.endsWith("SAPLING");
        }
    }
	
    public static void resetDamageDelay(final LivingEntity e) {
        new BukkitRunnable() {
            @Override
            public void run() {
                e.setNoDamageTicks(0);
            }
        }.runTaskLater(pl, 1L);
    }
    
    public static void setDespawnTimer(final Item item, final int ticks) {
        EntityItem nmsItem = (EntityItem) ((CraftItem) item).getHandle();
        try {
            Field f = nmsItem.getClass().getDeclaredField("age");
            f.setAccessible(true);
            f.set(nmsItem, nmsItem.world.spigotConfig.itemDespawnRate - ticks);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static String getTimeUntil(long then) {
    	return formatTimeDifference(then - System.currentTimeMillis());
    }
    
    public static String formatTimeDifference(long diff) {
        long sec = diff / 1000;
        long min = sec / 60;
        if (min == 0) {
            return sec + pluralify(" second", "s", sec);
        }
        
        long hour = min / 60;
        if (hour == 0) {
            sec = sec % 60;
            return min + pluralify(" minute", "s", min) + ", " + sec + pluralify(" second", "s", sec);
        }
        
        long day = hour / 24;
        if (day == 0) {
            min = min % 60;
            return hour + pluralify(" hour", "s", hour) + ", " + min + pluralify(" minute", "s", min);
        }
        
        long week = day / 7;
        if (week == 0) {
            hour = hour % 24;
            return day + pluralify(" day", "s", day) + ", " + hour + pluralify(" hour", "s", hour);
        }


        day = day % 7;
        return week + pluralify(" week", "s", week) + ", " + day + pluralify(" day", "s", day);
    }
    
    public static String pluralify(String singular, String suffix, long i) {
        return i > 1 ? singular + suffix : singular;
    }
    
    public static FileConfiguration saveAndGetResource(Plugin plugin, String file) {
        File FILE = new File("plugins" + File.separator + plugin.getName() + File.separator + file);
        if (!FILE.exists())
            plugin.saveResource(file, true);
        return YamlConfiguration.loadConfiguration(FILE);
    }
    
    public static void saveResource(Plugin plugin, FileConfiguration config, String name) {
		try {
			if (!name.contains("."))
				name = name + ".yml";
			config.save("plugins" + File.separator + plugin.getName() + File.separator + name);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public static void addPotionEffect(LivingEntity entity, PotionEffect effect) {
        PotionEffect cur = entity.getPotionEffect(effect.getType());
        if (cur != null) {
            if (cur.getAmplifier() < effect.getAmplifier()) {
                entity.addPotionEffect(effect, true);
            } else if (cur.getAmplifier() == effect.getAmplifier()) {
                if (effect.getDuration() > cur.getDuration())
                    entity.addPotionEffect(effect, true);
            }
            return;
        }
        entity.addPotionEffect(effect);
    }
    
    public static LivingEntity getActualDamager(Entity entity) {
        if (entity instanceof Projectile) {
            if (((Projectile) entity).getShooter() instanceof LivingEntity) {
                return (LivingEntity) ((Projectile) entity).getShooter();
            }
            return null;
        }
        return entity instanceof LivingEntity ? (LivingEntity) entity : null;
    }
    
    public static boolean checkEDBE(Entity causer, Entity victim) {
        return Utils.damageEventIsCancelled(causer, victim);
    }
    
    public static void damageProjectile(LivingEntity victim, Projectile proj, double amount) {
        net.minecraft.server.v1_13_R2.EntityLiving nmsEntity = ((CraftLivingEntity) victim).getHandle();
        net.minecraft.server.v1_13_R2.EntityProjectile nmsProj = ((CraftProjectile) proj).getHandle();
        nmsEntity.damageEntity(DamageSource.projectile(nmsProj, nmsProj.shooter), (float) amount);
    }
    
    public static void damageProjectile(LivingEntity victim, Arrow arrow, double amount) {
        net.minecraft.server.v1_13_R2.EntityLiving nmsEntity = ((CraftLivingEntity) victim).getHandle();
        net.minecraft.server.v1_13_R2.EntityArrow nmsProj = ((CraftArrow) arrow).getHandle();
        nmsEntity.damageEntity(DamageSource.projectile(nmsProj, nmsProj.getShooter()), (float) amount);
    }
    
    public static void damageProjectile(LivingEntity victim, LivingEntity attacker, double amount) {
        final Projectile proj = attacker.getWorld().spawn(attacker.getLocation().add(0, 100, 0), Egg.class);
        proj.setShooter(attacker);
        damageProjectile(victim, proj, amount);
        new BukkitRunnable() {
            @Override
            public void run() {
                proj.remove();
            }
        }.runTask(Utils.pl);
    }
    
    public static boolean isLivingEntity(Entity entity) {
        return entity instanceof LivingEntity && !(entity instanceof ArmorStand);
    }
    
    public static double heal(LivingEntity e, double amount) {
        double max = e.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        if (e.getHealth() >= max)
            return 0;
        
        double healed = Math.min(amount, max - e.getHealth());
        e.setHealth(e.getHealth() + healed);
        return healed;
    }
    
    public static List<net.minecraft.server.v1_13_R2.Entity> getWithinBox(Location loc, double x, double y, double z) {
        net.minecraft.server.v1_13_R2.World nmsWorld = ((CraftWorld) loc.getWorld()).getHandle();
        return nmsWorld.getEntities(null, new AxisAlignedBB(loc.getX() - x, loc.getY() - y, loc.getZ() - z, loc.getX() + x, loc.getY() + y, loc.getZ() + z));
    }
    
    public static ItemStack oneLess(ItemStack item) {
        int amt = item.getAmount() - 1;
        if (amt < 1) {
            return null;
        }
        
        return setItemAmount(item, amt);
    }
    
    public static void extendPotionEffect(LivingEntity p, PotionEffectType type, int level, int ticksExtend, boolean override, int maxDuration, boolean upgradeLevel, int maxLevel) {
        PotionEffect cur = p.getPotionEffect(type);
        if (cur == null) {
            p.addPotionEffect(new PotionEffect(type, ticksExtend, level));
            return;
        }
        
        int remaining = cur.getDuration();
        level = upgradeLevel ? Math.min(maxLevel, cur.getAmplifier() + 1) : level;
        p.addPotionEffect(new PotionEffect(type, Math.min(maxDuration, ticksExtend + remaining), level), override || cur.getAmplifier() <= level);
    }
    
    public static int clamp(int num, int min, int max) {
        if (num < min) {
            return min;
        }
        if (num > max) {
            return max;
        }
        return num;
    }
    
    public static double clamp(double num, double min, double max) {
        if (num < min) {
            return min;
        }
        if (num > max) {
            return max;
        }
        return num;
    }
    
    public static String getNameForEnchant(String enchname) {
        switch (enchname) {
        case "PROTECTION_ENVIRONMENTAL":
            return "Protection";
        case "PROTECTION_FALL":
            return "Feather Falling";
        case "OXYGEN":
            return "Respiration";
        case "WATER_WORKER":
            return "Aqua Affinity";
        case "THORNS":
            return "Thorns";
        case "DEPTH_STRIDER":
            return "Depth Strider";
        case "DAMAGE_ALL":
            return "Sharpness";
        case "KNOCKBACK":
            return "Knockback";
        case "FIRE_ASPECT":
            return "Fire Aspect";
        case "LOOT_BONUS_MOBS":
            return "Looting";
        case "DIG_SPEED":
            return "Efficiency";
        case "SILK_TOUCH":
            return "Silk Touch";
        case "DURABILITY":
            return "Unbreaking";
        case "LOOT_BONUS_BLOCKS":
            return "Fortune";
        case "ARROW_DAMAGE":
            return "Power";
        case "ARROW_KNOCKBACK":
            return "Punch";
        case "ARROW_FIRE":
            return "Flame";
        case "MENDING":
            return "Mending";
        case "FROST_WALKER":
            return "Frost Walker";
        case "ARROW_INFINITE":
            return "Infinity";
        case "LUCK":
            return "Luck of the Sea";
        case "LURE":
            return "Lure";
        case "DAMAGE_UNDEAD":
            return "Smite";
            default:
                return Utils.capitalizeEach(enchname).replace("_", " ");
        }
    }
    
    /**
     * @return Whether or not the player dies.
     */
    public static boolean damageUnblockable(LivingEntity e, double amt) {
    	float absorption = e instanceof Player ? ((CraftPlayer) e).getHandle().getAbsorptionHearts() : 0;
    	double health = e.getHealth();
    	
    	absorption -= amt;
    	if (e instanceof Player) {
    		((CraftPlayer) e).getHandle().setAbsorptionHearts(Math.max(absorption, 0));
    	}
    	
    	if (absorption > 0) {
            e.setNoDamageTicks(0);
            e.damage(0.001);
    		return false;
    	}
    	
        double left = health + absorption;
        if (left <= 0) {
            kill(e);
            return true;
        }
        e.setNoDamageTicks(0);
        e.setHealth(left);
        e.damage(0.001);
        return false;
    }
    
    public static void kill(LivingEntity e) {
        e.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
        e.setNoDamageTicks(0);
        e.setHealth(0.1);
        e.damage(20);
    }
    
    public static int getDurability(ItemStack item) {
    	if (item.getItemMeta() instanceof org.bukkit.inventory.meta.Damageable) {
    		return ((org.bukkit.inventory.meta.Damageable) item.getItemMeta()).getDamage();
    	}
    	return 0;
    }
    
    public static double distanceSquared(Location l1, Location l2) {
    	if (l1.getWorld() != l2.getWorld())
    		return Double.POSITIVE_INFINITY;
    	return l1.distanceSquared(l2);
    }
    
    public static double distance(Location l1, Location l2) {
    	if (l1.getWorld() != l2.getWorld())
    		return Double.POSITIVE_INFINITY;
    	return Math.sqrt(distanceSquared(l1, l2));
    }
    
    public static double distanceSquared2D(Location l1, Location l2) {
        double x1 = l1.getX(), z1 = l1.getZ(),
               x2 = l2.getX(), z2 = l2.getZ();
        boolean l2n = l2.getWorld().getName().endsWith("nether");
        boolean l1n = l1.getWorld().getName().endsWith("nether");
        if (l2n != l1n) {
            if (l1n) {
                x1 /= 8;
                z1 /= 8;
            } else {
                x2 /= 8;
                z2 /= 8;
            }
        }
        return Math.pow(x2 - x1, 2) + Math.pow(z2 - z1, 2);
    }

    public static boolean hasGroundUnderneath(Location behind, int reach) {
        behind = behind.clone();
        
        int j = 0;
        while (behind.getY() > 0 && j < reach) {
            Block b = behind.getBlock();
            if (passable(b)) {
                behind.add(0, -1, 0);
                ++j;
                continue;
            }
            return true;
        }
        return false;
    }
    
    public static boolean isMethylated(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.getLore();
        return lore != null && lore.contains(METHYL);
    }
    
    public static boolean methylate(ItemStack item) {
        if (isMethylated(item))
            return true;
        
        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.getLore();
        if (lore == null)
            lore = new ArrayList<>(1);
        lore.add(METHYL);
        meta.setLore(lore);
        item.setItemMeta(meta);
        return true;
    }
    
    public static void makeFearless(EntityInsentient nmsEntity) {
        PathfinderGoalSelector goalSelector = nmsEntity.goalSelector;
        Set<Object> set = (Set<Object>) ReflectionUtils.getPrivateField(goalSelector, "b"); // set<PathfinderGoalSelectionItem>
        for (Iterator<Object> it = set.iterator(); it.hasNext();) {
            Object o = it.next();
            // a field is a PathfinderGoal
            if (ReflectionUtils.getPrivateField(o, "a") instanceof PathfinderGoalAvoidTarget) {
                it.remove();
            }
        }
    }
    
    public static void damageMainhand(Player p, int amt) {
    	ItemStack item = p.getInventory().getItemInMainHand();
    	if (Utils.isEmpty(item))
    		return;

    	ItemMeta meta = item.getItemMeta();
    	if (!(item.getItemMeta() instanceof Damageable)) {
    		return;
    	}
    	
    	int unb = item.getEnchantmentLevel(Enchantment.DURABILITY);
    	if (unb > 0 && Math.random() > 1.0 / (unb + 1)) {
    		return;
    	}
    	
    	Damageable dmgb = (Damageable) meta;
    	int dmg = dmgb.getDamage() + amt;
    	if (dmg >= item.getType().getMaxDurability()) {
    		p.getInventory().setItemInMainHand(null);
    		p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0f, 1.0f);
    	} else {
    		dmgb.setDamage(dmg);
    		item.setItemMeta(meta);
    	}
    }
    
    public static void ground(Location loc, int max) {
    	int i = 0;
    	while (Math.abs(i) < max && Utils.passable(loc.getBlock().getRelative(0, i - 1, 0))) {
    		i -= 1;
    	}
    	loc.setY(loc.getY() + i);
    }
    
}
