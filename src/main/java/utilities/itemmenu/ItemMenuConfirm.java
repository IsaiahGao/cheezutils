package utilities.itemmenu;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import utilities.itembuilder.ItemBuilder;

public class ItemMenuConfirm {
	
	public ItemMenuConfirm(String title, MenuAction action, String... description) {
		this.menu = new ItemMenu(title, 4).setBackground(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setName("").build());
		ItemStack icon = new ItemBuilder(Material.LIME_STAINED_GLASS_PANE).setName("§a§lConfirm").setLore(description).build();
		this.menu.addIcon(12, icon, action);
		this.menu.addIcon(13, icon, action);
		this.menu.addIcon(14, icon, action);
		this.menu.addIcon(21, icon, action);
		this.menu.addIcon(22, icon, action);
		this.menu.addIcon(23, icon, action);
	}
	
	private ItemMenu menu;
	
	public void display(Player p) {
		this.menu.open(p);
	}

}
