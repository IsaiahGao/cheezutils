package utilities.itemmenu;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public interface MenuAction {
	
	void onClick(Inventory inv, int slot, Player p);

}
