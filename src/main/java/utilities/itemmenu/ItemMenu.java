package utilities.itemmenu;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import net.minecraft.server.v1_13_R2.ChatMessage;
import net.minecraft.server.v1_13_R2.EntityPlayer;
import net.minecraft.server.v1_13_R2.PacketPlayOutOpenWindow;
import utilities.Utils;

public class ItemMenu {
	
	public ItemMenu(String title, int rows) {
		int size = rows * 9;
		this.contents = new ItemStack[size];
		this.actionmap = new HashMap<>();
		this.title = title;
	}
	
	private String title;
	private ItemStack[] contents;
	private ItemStack background;
	private MenuAction onopen;
	private Map<Integer, MenuAction> actionmap;
	
	public ItemMenu setBackground(Material m) {
		return this.setBackground(new ItemStack(m));
	}
	
	public ItemMenu setBackground(ItemStack item) {
		background = Utils.setLore(Utils.setName(item, ""), "");
		for (int i = 0; i < contents.length; i++)
			if (contents[i] == null || contents[i].equals(background))
				contents[i] = background;
		return this;
	}
	
	public ItemMenu addIcon(int slot, ItemStack icon, MenuAction action) {
		actionmap.put(slot, action);
		contents[slot] = icon;
		return this;
	}
	
	public ItemMenu onOpen(MenuAction action) {
		this.onopen = action;
		return this;
	}
	
	public void changeTitle(Player viewer, String title) {
	    EntityPlayer ep = ((CraftPlayer) viewer).getHandle();
	    PacketPlayOutOpenWindow packet = new PacketPlayOutOpenWindow(ep.activeContainer.windowId, "minecraft:chest", new ChatMessage(title), viewer.getOpenInventory().getTopInventory().getSize());
	    ep.playerConnection.sendPacket(packet);
	    ep.updateInventory(ep.activeContainer);
	}
	
	public void open(Player p) {
		this.open(p, this.title);
	}
	
	public void open(final Player p, String title) {
		final Inventory inv = Bukkit.createInventory(null, contents.length, title);
		inv.setContents(contents);
		
		Listener list = new Listener() {
			@EventHandler
			public void inventoryClick(InventoryClickEvent e) {
				if (e.getWhoClicked() != p)
					return;
				
				if (!e.getView().getTopInventory().equals(inv))
					return;
				
				if (e.getClickedInventory() == null)
					return;
				
				e.setCancelled(true);
				if (!e.getClickedInventory().equals(e.getView().getTopInventory()))
					return;
				
				MenuAction ma = actionmap.get(e.getRawSlot());
				if (ma != null)
					ma.onClick(inv, e.getRawSlot(), p);
			}
			
			@EventHandler
			public void onLogout(PlayerQuitEvent e) {
				if (e.getPlayer() == p)
					HandlerList.unregisterAll(this);
			}

			@EventHandler
			public void onCloseInventory(InventoryCloseEvent e) {
				if (e.getPlayer() == p)
					HandlerList.unregisterAll(this);
			}
		};
		
		Bukkit.getPluginManager().registerEvents(list, Utils.pl);
		if (this.onopen != null) {
			this.onopen.onClick(inv, -1, p);
		}
		p.openInventory(inv);
	}
	
}
